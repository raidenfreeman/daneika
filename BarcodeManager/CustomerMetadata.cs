﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GkinisPetros
{
    [ProtoBuf.ProtoContract]
    public class CustomerMetadata
    {
        [ProtoBuf.ProtoMember(1)]
        public DateTime? earliestExpiration { get; set; }
        //[ProtoBuf.ProtoMember(2)]
        //public string name { get; set; }
        [ProtoBuf.ProtoMember(3)]
        public string code { get; set; }
        //[ProtoBuf.ProtoMember(4)]
        //public decimal totalOwed { get; set; }

        private CustomerMetadata()
        {

        }

        public CustomerMetadata(string code, DateTime? expiration)
        {
          //  this.name = name;
            this.code = code;
            //totalOwed = owed;
            earliestExpiration = expiration;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BarcodeLib;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

namespace GkinisPetros
{
    public class PrintingManager
    {
        const int barcodeHeight = 48;
        const int barcodeLength = 150;

        const int maxDescriptionLength = 49;

        const int rowHeight = 42;

        private const int xOffset = 15;
        private const int yOffset = 50;

        private readonly int numberOfFarmaka;
        private readonly int numberOfTransactions;
        private int itemIterator;

        //private PrintPreviewDialog previewDlg = null;
        private PrintDialog printDlg = null;
        private string[] stringArrayFarmaka;
        private string[] stringArrayTransactions;
        private Image[] barcodeFarmakonArray;
        private Image[] barcodeMonadikonKodikonArray;
        private Font printingFont;

        readonly decimal totalCost;
        readonly decimal totalCostFarmaka;
        readonly decimal totalCostXreostoumena;
        readonly Customer customer;

        public PrintingManager(Customer customer, decimal sumTotal, decimal sumFarmaka, decimal sumXreostoumena, List<DedomenaFarmakou> farmaka, List<CustomerTransactionData> transactions)
        {
            printingFont = new Font(new FontFamily("Courier New"), 9);

            numberOfFarmaka = farmaka.Count;
            barcodeFarmakonArray = new Image[numberOfFarmaka];
            barcodeMonadikonKodikonArray = new Image[numberOfFarmaka];
            stringArrayFarmaka = new string[numberOfFarmaka];

            this.customer = customer;
            int i = 0;
            foreach (var p in farmaka)
            {
                stringArrayFarmaka[i] = p.ApothikiCode.PadRight(5) + ' ' + p.Description + ' ' + p.Morfi;
                if (stringArrayFarmaka[i].Length > maxDescriptionLength)
                {
                    stringArrayFarmaka[i] = stringArrayFarmaka[i].Insert(maxDescriptionLength, ' ' + p.Price.ToString("C") + "\n");
                    if (stringArrayFarmaka[i].Length > maxDescriptionLength * 2 + 1 + p.Price.ToString("C").Length)
                        stringArrayFarmaka[i] = stringArrayFarmaka[i].Remove(maxDescriptionLength * 2);
                }
                else
                    stringArrayFarmaka[i] = stringArrayFarmaka[i].PadRight(maxDescriptionLength) + ' ' + p.Price.ToString("C");// + '\u20AC';
                var barcodeFarmakou = new Barcode();
                var barcodeMonadikouKodikou = new Barcode();
                barcodeFarmakou.IncludeLabel = true;
                barcodeMonadikouKodikou.IncludeLabel = true;
                try { barcodeFarmakonArray[i] = barcodeFarmakou.Encode(TYPE.EAN13, p.Barcode, barcodeLength, barcodeHeight); }
                catch (Exception)
                {
                    try { barcodeFarmakonArray[i] = barcodeFarmakou.Encode(TYPE.CODE128, p.Barcode, barcodeLength + 60, barcodeHeight); }
                    catch (Exception) { barcodeFarmakonArray[i] = DrawText(p.Barcode, printingFont, Color.Black, Color.White); }
                }

                if (p.UniqueBarcode != null && p.UniqueBarcode.Length == 12)
                {
                    try { barcodeMonadikonKodikonArray[i] = barcodeMonadikouKodikou.Encode(TYPE.CODE128, p.UniqueBarcode, barcodeLength, barcodeHeight); }
                    catch (Exception)
                    {
                        try { barcodeFarmakonArray[i] = barcodeMonadikouKodikou.Encode(TYPE.CODE93, p.UniqueBarcode, barcodeLength + 60, barcodeHeight); }
                        catch (Exception) { barcodeFarmakonArray[i] = DrawText(p.Barcode, printingFont, Color.Black, Color.White); }
                    }
                }
                else
                    barcodeMonadikonKodikonArray[i] = new Bitmap(1, 1);
                i++;
            }

            numberOfTransactions = transactions.Count;
            stringArrayTransactions = new string[numberOfTransactions];

            i = 0;
            foreach (var t in transactions)
            {
                stringArrayTransactions[i] = t.transactionCode;
                if (stringArrayTransactions[i].Length > maxDescriptionLength)
                    stringArrayTransactions[i] = stringArrayTransactions[i].Remove(maxDescriptionLength);// + t.money + '\u20AC';
                stringArrayTransactions[i] = stringArrayTransactions[i].PadRight(maxDescriptionLength) + " " + t.money.ToString("C");
                i++;
            }
            totalCost = sumTotal;
            totalCostFarmaka = sumFarmaka;
            totalCostXreostoumena = sumXreostoumena;
        }

        public bool PrintAll()
        {
            //Create a PrintPreviewDialog object
            printDlg = new PrintDialog();
            //Create a PrintDocument object
            PrintDocument pd = new PrintDocument();
            //Add print-page event handler
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            printDlg.Document = pd;
            DialogResult result = printDlg.ShowDialog();
            if (result == DialogResult.OK)
                //Print
                pd.Print();
            else
                return false;
            return true;
        }
        public bool PrintAllAuto()
        {
            //Create a PrintDocument object
            PrintDocument pd = new PrintDocument();
            //Add print-page event handler
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            //Print
            pd.Print();
            return true;
        }

        public void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            int iteratorLastValue = itemIterator;
            float ypos = yOffset;
            float pageheight = ev.MarginBounds.Bottom;
            //float pageLength = 740;
            float pageLength = ev.PageBounds.Width;// - xOffset;
            float pageBound = pageLength - xOffset - 12;
            //Create a Graphics object
            Graphics g = ev.Graphics;

            SolidBrush blackBrush = new SolidBrush(Color.Black);

            Pen blackPen = new Pen(Color.Black, 1);

            Font header = new Font(new FontFamily("Courier New"), 12, FontStyle.Bold);

            g.DrawString("\t\t\t       ΚΑΡΤΕΛΑ ΠΕΛΑΤΗ\t\t" + "             " + DateTime.Today.ToShortDateString(), header, blackBrush, xOffset, ypos - 2 * 17);
            g.DrawString(("ΟΝ/ΕΠΩΝΥΜΟ: " + (customer.FullName + " " + customer.PE_CODE)).PadRight(49) + " ΤΕΛΙΚΟ ΣΥΝΟΛΟ: " + totalCost.ToString("C"), header, blackBrush, xOffset, ypos - 17);
            //g.DrawString("ΤΜΧ", printingFont, blackBrush, xOffset, ypos + 2);
            g.DrawString("ΕΙΔΟΣ", printingFont, blackBrush, xOffset + 180, ypos + 2);
            g.DrawString("ΤΙΜΗ", printingFont, blackBrush, xOffset + 418, ypos + 2);
            g.DrawString("ΜΟΝΑΔΙΚΟΣ ΚΩΔΙΚΟΣ", printingFont, blackBrush, xOffset + 632, ypos + 2);
            //g.DrawString("ΥΠ.2", printingFont, blackBrush, xOffset + 575, ypos + 2);
            g.DrawString("BARCODE", printingFont, blackBrush, xOffset + 520, ypos + 2);

            g.DrawString("Εξοφλήθη:", header, blackBrush, xOffset, ypos - 34);
            g.DrawRectangle(blackPen, xOffset + 110, ypos - 35, 20, 20);

            g.DrawLine(blackPen, xOffset, ypos, pageBound - 2, ypos);

            ypos += 20;
            g.DrawLine(blackPen, xOffset, ypos, pageBound - 2, ypos);
            g.DrawLine(blackPen, xOffset, ypos - 2, pageBound - 2, ypos - 2);


            List<Tuple<int, float>> iteratorsToDrawLast = new List<Tuple<int, float>>();
            List<Tuple<int, float>> iteratorsToDrawLastUnique = new List<Tuple<int, float>>();
            for (itemIterator = iteratorLastValue; itemIterator < numberOfFarmaka && ypos + rowHeight < pageheight; itemIterator++)
            {
                g.DrawString(stringArrayFarmaka[itemIterator], printingFont, blackBrush, xOffset, ypos + 5);
                //if the barcode is not EAN-13 and does not fit, mark it to draw it above all else
                if (barcodeFarmakonArray[itemIterator].Size.Width > 100)
                    iteratorsToDrawLast.Add(new Tuple<int, float>(itemIterator, ypos));
                else //draw it normally if it's EAN-13
                    g.DrawImage(barcodeFarmakonArray[itemIterator], xOffset + 475, ypos + 1);
                //if the barcode is not EAN-13 and does not fit, mark it to draw it above all else
                if (barcodeMonadikonKodikonArray[itemIterator]?.Size.Width > 100)
                    iteratorsToDrawLastUnique.Add(new Tuple<int, float>(itemIterator, ypos));
                else //draw it normally if it's EAN-13
                    g.DrawImage(barcodeMonadikonKodikonArray[itemIterator], xOffset + 625, ypos + 1);
                ypos += rowHeight;
                g.DrawLine(blackPen, xOffset, ypos, pageBound - 2, ypos);
            }

            for (; itemIterator < numberOfFarmaka + numberOfTransactions && ypos + rowHeight < pageheight; itemIterator++)
            {
                g.DrawString(stringArrayTransactions[itemIterator - numberOfFarmaka], printingFont, blackBrush, xOffset, ypos + 5);
                ypos += rowHeight - 24;
                g.DrawLine(blackPen, xOffset, ypos, pageBound - 2, ypos);
            }

            g.DrawLine(blackPen, xOffset, yOffset, xOffset, ypos);
            g.DrawLine(blackPen, xOffset + 388, yOffset, xOffset + 388, ypos);
            g.DrawLine(blackPen, xOffset + 478, yOffset, xOffset + 478, ypos);
            //  g.DrawLine(blackPen, xOffset + 565, yOffset, xOffset + 565, ypos);
            g.DrawLine(blackPen, xOffset + 620, yOffset, xOffset + 620, ypos);
            g.DrawLine(blackPen, pageBound - 2, yOffset, pageBound - 2, ypos);

            //now draw the barcodes that are not EAN-13
            foreach (var itemToDraw in iteratorsToDrawLast)
                g.DrawImage(barcodeFarmakonArray[itemToDraw.Item1], pageBound - barcodeFarmakonArray[itemToDraw.Item1].Width - 150 - 2, itemToDraw.Item2);
            foreach (var itemToDraw in iteratorsToDrawLastUnique)
                g.DrawImage(barcodeMonadikonKodikonArray[itemToDraw.Item1], pageBound - barcodeMonadikonKodikonArray[itemToDraw.Item1].Width - 2, itemToDraw.Item2);

            if (itemIterator < numberOfFarmaka + numberOfTransactions || pageheight < ypos + 12)
                ev.HasMorePages = true;
            else
            {
                g.DrawRectangle(blackPen, xOffset, ypos, 148, 14);
                g.DrawRectangle(blackPen, xOffset + 148, ypos, 270, 14);
                g.DrawRectangle(blackPen, xOffset + 418, ypos, pageBound - xOffset - 418 - 2, 14);
                Font sumFont = new Font(new FontFamily("Courier New"), 9, FontStyle.Bold);
                g.DrawString("Σύνολο ειδών: " + numberOfFarmaka.ToString().PadRight(5) + "Σύνολο Λοιπών Οφειλών: " + totalCostXreostoumena.ToString("C").PadRight(12) + "Σύνολο Φαρμάκων & Παραφαρμάκων: " + totalCostFarmaka.ToString("C"), sumFont, blackBrush, xOffset, ypos + 1);
            }
        }

        private Image DrawText(String text, Font font, Color textColor, Color backColor)
        {
            //first, create a dummy bitmap just to get a graphics object
            Image img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);

            //measure the string to see how big the image needs to be
            SizeF textSize = drawing.MeasureString(text, font);

            //free up the dummy image and old graphics object
            img.Dispose();
            drawing.Dispose();

            //create a new image of the right size
            img = new Bitmap((int)textSize.Width, (int)textSize.Height);

            drawing = Graphics.FromImage(img);

            //paint the background
            drawing.Clear(backColor);

            //create a brush for the text
            Brush textBrush = new SolidBrush(textColor);

            drawing.DrawString(text, font, textBrush, 0, 0);

            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            return img;

        }
    }
}

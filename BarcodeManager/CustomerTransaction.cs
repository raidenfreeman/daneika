﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GkinisPetros
{
    [ProtoBuf.ProtoContract]
    public class CustomerTransactionData
    {
        [ProtoBuf.ProtoMember(1)]
        public decimal money { get; set; }
        [ProtoBuf.ProtoMember(2)]
        public string transactionCode { get; set; }

        public CustomerTransactionData()
        {
            money = 0;
            transactionCode = "";
        }

        public CustomerTransactionData(decimal money, string code)
        {
            this.money = money;
            transactionCode = code;
        }
    }
}

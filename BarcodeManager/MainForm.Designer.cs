﻿namespace GkinisPetros
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.AddToListButton = new System.Windows.Forms.Button();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.barcodeTextBox = new System.Windows.Forms.TextBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.uniqueBarcodeTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxDelete = new System.Windows.Forms.TextBox();
            this.productDataGridView = new System.Windows.Forms.DataGridView();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewCoupons = new System.Windows.Forms.DataGridView();
            this.Morfi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UniqueBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpirationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PurchaseDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barcodeProductBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.αρχείοToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.φόρτωσηΔεδομένωνToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.αποθήκευσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.αποθήκευσηΩςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.έξοδοςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ρυθμίσειςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ενημέρωσηΠελατώνToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ξεκλείδωμαΠελάτηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκκίνησηΕκτύπωσηςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκτύπωσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupΣεΦάκελοToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.επαναφοράΑπόBackupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barcodeErrorTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.expirationButton = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.autoExpirationButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.priceErrorToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.uniqueBarcodeToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.showProdListButton = new System.Windows.Forms.Button();
            this.barcodeProductBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.barcodeProductBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sumLabel = new System.Windows.Forms.Label();
            this.sumTextBoxCoupons = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dateErrorTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxCount = new System.Windows.Forms.TextBox();
            this.showPrintedList = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxCustomer = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewTransactions = new System.Windows.Forms.DataGridView();
            this.Delete = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.sumTextBoxTransactions = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxSumTotal = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.listBoxPhones = new System.Windows.Forms.ListBox();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxCode = new System.Windows.Forms.TextBox();
            this.folderBrowserDialogNetwork = new System.Windows.Forms.FolderBrowserDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pELATEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productBarcodeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dedomenaFarmakouBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.moneyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transactionCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerTransactionDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dedomenaFarmakouBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dedomenaFarmakouBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCoupons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcodeProductBindingSource2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barcodeProductBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcodeProductBindingSource)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTransactions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pELATEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBarcodeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedomenaFarmakouBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerTransactionDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedomenaFarmakouBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedomenaFarmakouBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Aquamarine;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.AddToListButton, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.maskedTextBox1, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.barcodeTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.priceTextBox, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.uniqueBarcodeTextBox, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.comboBox1, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 40);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1092, 102);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // AddToListButton
            // 
            this.AddToListButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.AddToListButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.AddToListButton.Location = new System.Drawing.Point(945, 49);
            this.AddToListButton.Name = "AddToListButton";
            this.AddToListButton.Size = new System.Drawing.Size(87, 24);
            this.AddToListButton.TabIndex = 4;
            this.AddToListButton.Tag = "InputControlTag";
            this.AddToListButton.Text = "Προσθήκη";
            this.AddToListButton.UseVisualStyleBackColor = true;
            this.AddToListButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.maskedTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.maskedTextBox1.Location = new System.Drawing.Point(555, 48);
            this.maskedTextBox1.Mask = "00/00/0000";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(162, 26);
            this.maskedTextBox1.TabIndex = 2;
            this.maskedTextBox1.Tag = "InputControlTag";
            this.maskedTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.maskedTextBox1.ValidatingType = typeof(System.DateTime);
            this.maskedTextBox1.TextChanged += new System.EventHandler(this.maskedTextBox1_TextChanged);
            this.maskedTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBox1_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label4.Location = new System.Drawing.Point(724, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Τιμή";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(555, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ημερομηνία Λήξης";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(391, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Μοναδικό Barcode";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // barcodeTextBox
            // 
            this.barcodeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.barcodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.barcodeTextBox.Location = new System.Drawing.Point(255, 48);
            this.barcodeTextBox.MaxLength = 13;
            this.barcodeTextBox.Name = "barcodeTextBox";
            this.barcodeTextBox.Size = new System.Drawing.Size(129, 26);
            this.barcodeTextBox.TabIndex = 0;
            this.barcodeTextBox.Tag = "InputControlTag";
            this.barcodeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.barcodeTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.barcodeTextBox_KeyDown);
            // 
            // priceTextBox
            // 
            this.priceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.priceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.priceTextBox.Location = new System.Drawing.Point(724, 48);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(159, 26);
            this.priceTextBox.TabIndex = 3;
            this.priceTextBox.TabStop = false;
            this.priceTextBox.Tag = "InputControlTag";
            this.priceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.priceTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.priceTextBox_KeyPress);
            this.priceTextBox.Leave += new System.EventHandler(this.priceTextBox_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(255, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Barcode";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uniqueBarcodeTextBox
            // 
            this.uniqueBarcodeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uniqueBarcodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.uniqueBarcodeTextBox.Location = new System.Drawing.Point(391, 48);
            this.uniqueBarcodeTextBox.MaxLength = 12;
            this.uniqueBarcodeTextBox.Name = "uniqueBarcodeTextBox";
            this.uniqueBarcodeTextBox.Size = new System.Drawing.Size(157, 26);
            this.uniqueBarcodeTextBox.TabIndex = 1;
            this.uniqueBarcodeTextBox.Tag = "InputControlTag";
            this.uniqueBarcodeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.uniqueBarcodeTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uniqueBarcodeTextBox_KeyPress);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label5.Location = new System.Drawing.Point(890, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(198, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Αποθήκευση";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Aquamarine;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBoxDelete, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1496, 32);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.80582F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.19418F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(167, 103);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label6.Location = new System.Drawing.Point(4, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 39);
            this.label6.TabIndex = 9;
            this.label6.Text = "Διαγραφή Barcode";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxDelete
            // 
            this.textBoxDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.textBoxDelete.Location = new System.Drawing.Point(4, 58);
            this.textBoxDelete.MaxLength = 12;
            this.textBoxDelete.Name = "textBoxDelete";
            this.textBoxDelete.Size = new System.Drawing.Size(159, 26);
            this.textBoxDelete.TabIndex = 6;
            this.textBoxDelete.Tag = "InputControlTag";
            this.textBoxDelete.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxDelete.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxDelete_KeyPress);
            // 
            // productDataGridView
            // 
            this.productDataGridView.AllowUserToAddRows = false;
            this.productDataGridView.AllowUserToDeleteRows = false;
            this.productDataGridView.AllowUserToOrderColumns = true;
            this.productDataGridView.AllowUserToResizeRows = false;
            this.productDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.productDataGridView.AutoGenerateColumns = false;
            this.productDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.productDataGridView.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.productDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.productDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.productDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.productDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.productDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn25});
            this.productDataGridView.DataSource = this.productBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.productDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.productDataGridView.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.productDataGridView.Location = new System.Drawing.Point(0, 19);
            this.productDataGridView.Name = "productDataGridView";
            this.productDataGridView.ReadOnly = true;
            this.productDataGridView.RowHeadersWidth = 40;
            this.productDataGridView.Size = new System.Drawing.Size(801, 68);
            this.productDataGridView.TabIndex = 1;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // dataGridViewCoupons
            // 
            this.dataGridViewCoupons.AllowUserToAddRows = false;
            this.dataGridViewCoupons.AllowUserToOrderColumns = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCoupons.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewCoupons.AutoGenerateColumns = false;
            this.dataGridViewCoupons.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridViewCoupons.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.dataGridViewCoupons.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewCoupons.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridViewCoupons.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCoupons.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewCoupons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCoupons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.descriptionDataGridViewTextBoxColumn,
            this.Morfi,
            this.barcodeDataGridViewTextBoxColumn,
            this.UniqueBarcode,
            this.Price,
            this.ExpirationDate,
            this.PurchaseDate,
            this.Column1});
            this.dataGridViewCoupons.DataSource = this.dedomenaFarmakouBindingSource2;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LimeGreen;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCoupons.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewCoupons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCoupons.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridViewCoupons.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewCoupons.Name = "dataGridViewCoupons";
            this.dataGridViewCoupons.RowHeadersWidth = 40;
            this.dataGridViewCoupons.Size = new System.Drawing.Size(1662, 473);
            this.dataGridViewCoupons.TabIndex = 3;
            this.dataGridViewCoupons.Tag = "InputControlTag";
            this.dataGridViewCoupons.DataSourceChanged += new System.EventHandler(this.dataGridView2_DataSourceChanged);
            this.dataGridViewCoupons.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            this.dataGridViewCoupons.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellEndEdit);
            this.dataGridViewCoupons.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellValueChanged);
            this.dataGridViewCoupons.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewCoupons_DataError);
            this.dataGridViewCoupons.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView2_RowsAdded);
            this.dataGridViewCoupons.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView2_RowsRemoved);
            this.dataGridViewCoupons.Sorted += new System.EventHandler(this.dataGridView2_Sorted);
            // 
            // Morfi
            // 
            this.Morfi.DataPropertyName = "Morfi";
            this.Morfi.HeaderText = "Μορφή";
            this.Morfi.Name = "Morfi";
            this.Morfi.ReadOnly = true;
            this.Morfi.Width = 110;
            // 
            // UniqueBarcode
            // 
            this.UniqueBarcode.DataPropertyName = "UniqueBarcode";
            this.UniqueBarcode.HeaderText = "Μοναδικό Barcode";
            this.UniqueBarcode.Name = "UniqueBarcode";
            this.UniqueBarcode.ReadOnly = true;
            this.UniqueBarcode.Width = 232;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            dataGridViewCellStyle8.Format = "C2";
            dataGridViewCellStyle8.NullValue = null;
            this.Price.DefaultCellStyle = dataGridViewCellStyle8;
            this.Price.HeaderText = "Τιμή";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 83;
            // 
            // ExpirationDate
            // 
            this.ExpirationDate.DataPropertyName = "ExpirationDate";
            this.ExpirationDate.HeaderText = "Ημ. Λήξης";
            this.ExpirationDate.Name = "ExpirationDate";
            this.ExpirationDate.Width = 144;
            // 
            // Column1
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle9.NullValue = false;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle9;
            this.Column1.HeaderText = "Ολοκληρώθηκε";
            this.Column1.Name = "Column1";
            this.Column1.Width = 175;
            // 
            // PurchaseDate
            // 
            this.PurchaseDate.DataPropertyName = "PurchaseDate";
            this.PurchaseDate.HeaderText = "Ημ. Πώλησης";
            this.PurchaseDate.Name = "PurchaseDate";
            this.PurchaseDate.Width = 174;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.αρχείοToolStripMenuItem,
            this.ρυθμίσειςToolStripMenuItem,
            this.εκκίνησηΕκτύπωσηςToolStripMenuItem,
            this.εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem,
            this.εκτύπωσηToolStripMenuItem,
            this.backupToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1676, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // αρχείοToolStripMenuItem
            // 
            this.αρχείοToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.φόρτωσηΔεδομένωνToolStripMenuItem,
            this.αποθήκευσηToolStripMenuItem,
            this.αποθήκευσηΩςToolStripMenuItem,
            this.φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem,
            this.έξοδοςToolStripMenuItem});
            this.αρχείοToolStripMenuItem.Name = "αρχείοToolStripMenuItem";
            this.αρχείοToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.αρχείοToolStripMenuItem.Text = "Αρχείο";
            // 
            // φόρτωσηΔεδομένωνToolStripMenuItem
            // 
            this.φόρτωσηΔεδομένωνToolStripMenuItem.Name = "φόρτωσηΔεδομένωνToolStripMenuItem";
            this.φόρτωσηΔεδομένωνToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.φόρτωσηΔεδομένωνToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.φόρτωσηΔεδομένωνToolStripMenuItem.Text = "Φόρτωση Δεδομένων";
            this.φόρτωσηΔεδομένωνToolStripMenuItem.Click += new System.EventHandler(this.φόρτωσηΔεδομένωνToolStripMenuItem_Click);
            // 
            // αποθήκευσηToolStripMenuItem
            // 
            this.αποθήκευσηToolStripMenuItem.Name = "αποθήκευσηToolStripMenuItem";
            this.αποθήκευσηToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.αποθήκευσηToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.αποθήκευσηToolStripMenuItem.Text = "Αποθήκευση";
            this.αποθήκευσηToolStripMenuItem.Click += new System.EventHandler(this.αποθήκευσηToolStripMenuItem_Click);
            // 
            // αποθήκευσηΩςToolStripMenuItem
            // 
            this.αποθήκευσηΩςToolStripMenuItem.Enabled = false;
            this.αποθήκευσηΩςToolStripMenuItem.Name = "αποθήκευσηΩςToolStripMenuItem";
            this.αποθήκευσηΩςToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.αποθήκευσηΩςToolStripMenuItem.Text = "Αποθήκευση Ως";
            // 
            // φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem
            // 
            this.φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem.Enabled = false;
            this.φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem.Name = "φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem";
            this.φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem.Text = "Φόρτωση Εφεδρικού Αρχείου";
            // 
            // έξοδοςToolStripMenuItem
            // 
            this.έξοδοςToolStripMenuItem.Name = "έξοδοςToolStripMenuItem";
            this.έξοδοςToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.έξοδοςToolStripMenuItem.Text = "Έξοδος";
            this.έξοδοςToolStripMenuItem.Click += new System.EventHandler(this.έξοδοςToolStripMenuItem_Click);
            // 
            // ρυθμίσειςToolStripMenuItem
            // 
            this.ρυθμίσειςToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem,
            this.επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem,
            this.ενημέρωσηΠελατώνToolStripMenuItem,
            this.ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem,
            this.ξεκλείδωμαΠελάτηToolStripMenuItem});
            this.ρυθμίσειςToolStripMenuItem.Name = "ρυθμίσειςToolStripMenuItem";
            this.ρυθμίσειςToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.ρυθμίσειςToolStripMenuItem.Text = "Ρυθμίσεις";
            // 
            // αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem
            // 
            this.αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem.Name = "αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem";
            this.αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem.Text = "Αλλαγή Βάσης Δεδομένων Farmakon";
            this.αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem.Click += new System.EventHandler(this.αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem_Click);
            // 
            // επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem
            // 
            this.επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem.Name = "επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem";
            this.επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem.Text = "Επιλογή Χρήστη Βάσης Δεδομένων";
            this.επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem.Click += new System.EventHandler(this.επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem_Click);
            // 
            // ενημέρωσηΠελατώνToolStripMenuItem
            // 
            this.ενημέρωσηΠελατώνToolStripMenuItem.Name = "ενημέρωσηΠελατώνToolStripMenuItem";
            this.ενημέρωσηΠελατώνToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.ενημέρωσηΠελατώνToolStripMenuItem.Text = "Ενημέρωση Πελατών";
            this.ενημέρωσηΠελατώνToolStripMenuItem.Click += new System.EventHandler(this.ενημέρωσηΠελατώνToolStripMenuItem_Click);
            // 
            // ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem
            // 
            this.ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem.Name = "ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem";
            this.ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem.Text = "Ορισμός Θέσης Αποθήκευσης";
            this.ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem.Click += new System.EventHandler(this.ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem_Click);
            // 
            // ξεκλείδωμαΠελάτηToolStripMenuItem
            // 
            this.ξεκλείδωμαΠελάτηToolStripMenuItem.Name = "ξεκλείδωμαΠελάτηToolStripMenuItem";
            this.ξεκλείδωμαΠελάτηToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.ξεκλείδωμαΠελάτηToolStripMenuItem.Text = "Ξεκλείδωμα Πελάτη";
            this.ξεκλείδωμαΠελάτηToolStripMenuItem.Click += new System.EventHandler(this.ξεκλείδωμαΠελάτηToolStripMenuItem_Click);
            // 
            // εκκίνησηΕκτύπωσηςToolStripMenuItem
            // 
            this.εκκίνησηΕκτύπωσηςToolStripMenuItem.Name = "εκκίνησηΕκτύπωσηςToolStripMenuItem";
            this.εκκίνησηΕκτύπωσηςToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.εκκίνησηΕκτύπωσηςToolStripMenuItem.Size = new System.Drawing.Size(186, 20);
            this.εκκίνησηΕκτύπωσηςToolStripMenuItem.Text = "Εκτύπωση Barcodes σε LP 2824";
            this.εκκίνησηΕκτύπωσηςToolStripMenuItem.Click += new System.EventHandler(this.εκκίνησηΕκτύπωσηςToolStripMenuItem_Click_1);
            // 
            // εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem
            // 
            this.εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem.Name = "εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem";
            this.εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem.Size = new System.Drawing.Size(216, 20);
            this.εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem.Text = "Εκτύπωση Barcodes σε LP 2824 PLUS";
            this.εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem.Click += new System.EventHandler(this.εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem_Click);
            // 
            // εκτύπωσηToolStripMenuItem
            // 
            this.εκτύπωσηToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem});
            this.εκτύπωσηToolStripMenuItem.Name = "εκτύπωσηToolStripMenuItem";
            this.εκτύπωσηToolStripMenuItem.Size = new System.Drawing.Size(129, 20);
            this.εκτύπωσηToolStripMenuItem.Text = "Εκτύπωση Οφειλών";
            // 
            // οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem
            // 
            this.οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem.Name = "οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem";
            this.οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem.Text = "Οφειλές Τρέχοντος Πελάτη";
            this.οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem.Click += new System.EventHandler(this.οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem_Click);
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupΣεΦάκελοToolStripMenuItem,
            this.επαναφοράΑπόBackupToolStripMenuItem});
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.backupToolStripMenuItem.Text = "Backup";
            // 
            // backupΣεΦάκελοToolStripMenuItem
            // 
            this.backupΣεΦάκελοToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("backupΣεΦάκελοToolStripMenuItem.Image")));
            this.backupΣεΦάκελοToolStripMenuItem.Name = "backupΣεΦάκελοToolStripMenuItem";
            this.backupΣεΦάκελοToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.backupΣεΦάκελοToolStripMenuItem.Text = "Backup Σε Φάκελο...";
            this.backupΣεΦάκελοToolStripMenuItem.Click += new System.EventHandler(this.backupΣεΦάκελοToolStripMenuItem_Click);
            // 
            // επαναφοράΑπόBackupToolStripMenuItem
            // 
            this.επαναφοράΑπόBackupToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("επαναφοράΑπόBackupToolStripMenuItem.Image")));
            this.επαναφοράΑπόBackupToolStripMenuItem.Name = "επαναφοράΑπόBackupToolStripMenuItem";
            this.επαναφοράΑπόBackupToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.επαναφοράΑπόBackupToolStripMenuItem.Text = "Επαναφορά Από Backup";
            this.επαναφοράΑπόBackupToolStripMenuItem.Click += new System.EventHandler(this.επαναφοράΑπόBackupToolStripMenuItem_Click);
            // 
            // expirationButton
            // 
            this.expirationButton.Location = new System.Drawing.Point(6, 23);
            this.expirationButton.Name = "expirationButton";
            this.expirationButton.Size = new System.Drawing.Size(75, 39);
            this.expirationButton.TabIndex = 2;
            this.expirationButton.Text = "Λήγουν:";
            this.expirationButton.UseVisualStyleBackColor = true;
            this.expirationButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker2.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.dateTimePicker2.CustomFormat = "";
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(87, 27);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(128, 26);
            this.dateTimePicker2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.PapayaWhip;
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.autoExpirationButton);
            this.groupBox1.Controls.Add(this.expirationButton);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.groupBox1.Location = new System.Drawing.Point(1111, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(229, 116);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Αναζήτηση Λήξης";
            // 
            // autoExpirationButton
            // 
            this.autoExpirationButton.Location = new System.Drawing.Point(6, 68);
            this.autoExpirationButton.Name = "autoExpirationButton";
            this.autoExpirationButton.Size = new System.Drawing.Size(209, 39);
            this.autoExpirationButton.TabIndex = 0;
            this.autoExpirationButton.Text = "Λήγουν αυτό το Μήνα";
            this.autoExpirationButton.UseVisualStyleBackColor = true;
            this.autoExpirationButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.productDataGridView);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.groupBox2.Location = new System.Drawing.Point(2, 143);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(807, 93);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Βάση Δεδομένων Farmakon.net";
            // 
            // showProdListButton
            // 
            this.showProdListButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.showProdListButton.Location = new System.Drawing.Point(1346, 89);
            this.showProdListButton.Name = "showProdListButton";
            this.showProdListButton.Size = new System.Drawing.Size(144, 53);
            this.showProdListButton.TabIndex = 2;
            this.showProdListButton.Text = "Προβολή Αποθηκευμένων";
            this.showProdListButton.UseVisualStyleBackColor = true;
            this.showProdListButton.Click += new System.EventHandler(this.showProdListButton_Click);
            // 
            // sumLabel
            // 
            this.sumLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sumLabel.AutoSize = true;
            this.sumLabel.BackColor = System.Drawing.Color.Transparent;
            this.sumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.sumLabel.Location = new System.Drawing.Point(3, 10);
            this.sumLabel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.sumLabel.Name = "sumLabel";
            this.sumLabel.Size = new System.Drawing.Size(106, 29);
            this.sumLabel.TabIndex = 2;
            this.sumLabel.Text = "Σύνολο :";
            this.sumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sumTextBoxCoupons
            // 
            this.sumTextBoxCoupons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sumTextBoxCoupons.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.sumTextBoxCoupons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sumTextBoxCoupons.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.sumTextBoxCoupons.ForeColor = System.Drawing.Color.White;
            this.sumTextBoxCoupons.Location = new System.Drawing.Point(115, 6);
            this.sumTextBoxCoupons.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.sumTextBoxCoupons.Name = "sumTextBoxCoupons";
            this.sumTextBoxCoupons.ReadOnly = true;
            this.sumTextBoxCoupons.Size = new System.Drawing.Size(255, 35);
            this.sumTextBoxCoupons.TabIndex = 3;
            this.sumTextBoxCoupons.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sumTextBoxCoupons.TextChanged += new System.EventHandler(this.sumTextBox_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Aquamarine;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.sumLabel);
            this.flowLayoutPanel1.Controls.Add(this.sumTextBoxCoupons);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(376, 49);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "mdf";
            this.openFileDialog1.Filter = "Βάσεις Δεδομένων|*.mdf|Όλα τα αρχεία|*.*";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.Aquamarine;
            this.flowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel2.Controls.Add(this.label7);
            this.flowLayoutPanel2.Controls.Add(this.textBoxCount);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 67);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(376, 49);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label7.Location = new System.Drawing.Point(3, 10);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 29);
            this.label7.TabIndex = 2;
            this.label7.Text = "Πλήθος :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxCount
            // 
            this.textBoxCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCount.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.textBoxCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.textBoxCount.ForeColor = System.Drawing.Color.White;
            this.textBoxCount.Location = new System.Drawing.Point(118, 6);
            this.textBoxCount.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.textBoxCount.Name = "textBoxCount";
            this.textBoxCount.ReadOnly = true;
            this.textBoxCount.Size = new System.Drawing.Size(252, 35);
            this.textBoxCount.TabIndex = 3;
            this.textBoxCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // showPrintedList
            // 
            this.showPrintedList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.showPrintedList.Location = new System.Drawing.Point(1346, 32);
            this.showPrintedList.Name = "showPrintedList";
            this.showPrintedList.Size = new System.Drawing.Size(144, 54);
            this.showPrintedList.TabIndex = 7;
            this.showPrintedList.Text = "Προβολή Εκτυπωμένων";
            this.showPrintedList.UseVisualStyleBackColor = true;
            this.showPrintedList.Click += new System.EventHandler(this.showPrintedList_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.Aquamarine;
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.label8);
            this.flowLayoutPanel3.Controls.Add(this.comboBoxCustomer);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(2, 261);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(633, 49);
            this.flowLayoutPanel3.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label8.Location = new System.Drawing.Point(3, 11);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 29);
            this.label8.TabIndex = 1;
            this.label8.Text = "Πελάτης :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxCustomer
            // 
            this.comboBoxCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.comboBoxCustomer.FormattingEnabled = true;
            this.comboBoxCustomer.Location = new System.Drawing.Point(133, 6);
            this.comboBoxCustomer.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.comboBoxCustomer.Name = "comboBoxCustomer";
            this.comboBoxCustomer.Size = new System.Drawing.Size(491, 37);
            this.comboBoxCustomer.TabIndex = 0;
            this.comboBoxCustomer.SelectedIndexChanged += new System.EventHandler(this.comboBoxCustomer_SelectedIndexChanged);
            this.comboBoxCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyDown);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 383);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1676, 505);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.PapayaWhip;
            this.tabPage1.Controls.Add(this.dataGridViewCoupons);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1668, 479);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Φάρμακα & Παραφάρμακα";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.PapayaWhip;
            this.tabPage2.Controls.Add(this.dataGridViewTransactions);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1668, 479);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Λοιπά Χρεωστούμενα";
            // 
            // dataGridViewTransactions
            // 
            this.dataGridViewTransactions.AllowUserToDeleteRows = false;
            this.dataGridViewTransactions.AllowUserToOrderColumns = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTransactions.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTransactions.AutoGenerateColumns = false;
            this.dataGridViewTransactions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridViewTransactions.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.dataGridViewTransactions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewTransactions.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridViewTransactions.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTransactions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTransactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.moneyDataGridViewTextBoxColumn,
            this.transactionCodeDataGridViewTextBoxColumn,
            this.Delete});
            this.dataGridViewTransactions.DataSource = this.customerTransactionDataBindingSource;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.LimeGreen;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTransactions.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTransactions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTransactions.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridViewTransactions.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTransactions.Name = "dataGridViewTransactions";
            this.dataGridViewTransactions.RowHeadersWidth = 40;
            this.dataGridViewTransactions.Size = new System.Drawing.Size(1662, 473);
            this.dataGridViewTransactions.TabIndex = 4;
            this.dataGridViewTransactions.Tag = "InputControlTag";
            this.dataGridViewTransactions.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTransactions_CellContentClick);
            this.dataGridViewTransactions.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTransactions_CellEndEdit);
            this.dataGridViewTransactions.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTransactions_CellValueChanged);
            this.dataGridViewTransactions.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewTransactions_RowsAdded);
            this.dataGridViewTransactions.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridViewTransactions_RowsRemoved);
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Διαγραφή";
            this.Delete.Name = "Delete";
            this.Delete.Width = 118;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.flowLayoutPanel1);
            this.groupBox3.Controls.Add(this.flowLayoutPanel2);
            this.groupBox3.Location = new System.Drawing.Point(1051, 148);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(388, 127);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Φάρμακα και Παραφάρμακα";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.flowLayoutPanel4);
            this.groupBox4.Location = new System.Drawing.Point(1035, 289);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(388, 78);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Λοιπά Χρεωστούμενα";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BackColor = System.Drawing.Color.Aquamarine;
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Controls.Add(this.label9);
            this.flowLayoutPanel4.Controls.Add(this.sumTextBoxTransactions);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(6, 19);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(376, 49);
            this.flowLayoutPanel4.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label9.Location = new System.Drawing.Point(3, 10);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 29);
            this.label9.TabIndex = 2;
            this.label9.Text = "Σύνολο :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sumTextBoxTransactions
            // 
            this.sumTextBoxTransactions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sumTextBoxTransactions.BackColor = System.Drawing.Color.Green;
            this.sumTextBoxTransactions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sumTextBoxTransactions.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.sumTextBoxTransactions.ForeColor = System.Drawing.Color.White;
            this.sumTextBoxTransactions.Location = new System.Drawing.Point(115, 6);
            this.sumTextBoxTransactions.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.sumTextBoxTransactions.Name = "sumTextBoxTransactions";
            this.sumTextBoxTransactions.ReadOnly = true;
            this.sumTextBoxTransactions.Size = new System.Drawing.Size(255, 35);
            this.sumTextBoxTransactions.TabIndex = 3;
            this.sumTextBoxTransactions.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sumTextBoxTransactions.TextChanged += new System.EventHandler(this.sumTextBoxTransactions_TextChanged);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BackColor = System.Drawing.Color.Aquamarine;
            this.flowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel6.Controls.Add(this.label11);
            this.flowLayoutPanel6.Controls.Add(this.textBoxSumTotal);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(3, 316);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(632, 49);
            this.flowLayoutPanel6.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label11.Location = new System.Drawing.Point(3, 10);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(192, 29);
            this.label11.TabIndex = 2;
            this.label11.Text = "Σύνολο Πελάτη:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxSumTotal
            // 
            this.textBoxSumTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSumTotal.BackColor = System.Drawing.Color.MediumBlue;
            this.textBoxSumTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSumTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.textBoxSumTotal.ForeColor = System.Drawing.Color.White;
            this.textBoxSumTotal.Location = new System.Drawing.Point(201, 6);
            this.textBoxSumTotal.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.textBoxSumTotal.Name = "textBoxSumTotal";
            this.textBoxSumTotal.ReadOnly = true;
            this.textBoxSumTotal.Size = new System.Drawing.Size(422, 35);
            this.textBoxSumTotal.TabIndex = 3;
            this.textBoxSumTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.button1.Location = new System.Drawing.Point(815, 152);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 84);
            this.button1.TabIndex = 12;
            this.button1.Text = "Πελάτες με ληξιπρόθεσμες οφειλές αυτό το μήνα";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.PapayaWhip;
            this.groupBox5.Controls.Add(this.flowLayoutPanel7);
            this.groupBox5.Controls.Add(this.flowLayoutPanel8);
            this.groupBox5.Location = new System.Drawing.Point(641, 242);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(388, 125);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Πληροφορίες Πελάτη";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.BackColor = System.Drawing.Color.Aquamarine;
            this.flowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel7.Controls.Add(this.label12);
            this.flowLayoutPanel7.Controls.Add(this.listBoxPhones);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(6, 19);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(376, 49);
            this.flowLayoutPanel7.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label12.Location = new System.Drawing.Point(3, 10);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 29);
            this.label12.TabIndex = 2;
            this.label12.Text = "Τηλ:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxPhones
            // 
            this.listBoxPhones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxPhones.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.listBoxPhones.FormattingEnabled = true;
            this.listBoxPhones.ItemHeight = 29;
            this.listBoxPhones.Location = new System.Drawing.Point(71, 8);
            this.listBoxPhones.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.listBoxPhones.Name = "listBoxPhones";
            this.listBoxPhones.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxPhones.Size = new System.Drawing.Size(300, 33);
            this.listBoxPhones.TabIndex = 3;
            this.listBoxPhones.UseTabStops = false;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.BackColor = System.Drawing.Color.Aquamarine;
            this.flowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel8.Controls.Add(this.label13);
            this.flowLayoutPanel8.Controls.Add(this.textBoxCode);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(6, 67);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(376, 49);
            this.flowLayoutPanel8.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label13.Location = new System.Drawing.Point(3, 10);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 29);
            this.label13.TabIndex = 2;
            this.label13.Text = "Κωδικός:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxCode
            // 
            this.textBoxCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCode.BackColor = System.Drawing.Color.DarkSlateGray;
            this.textBoxCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.textBoxCode.ForeColor = System.Drawing.Color.White;
            this.textBoxCode.Location = new System.Drawing.Point(119, 6);
            this.textBoxCode.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.textBoxCode.Name = "textBoxCode";
            this.textBoxCode.ReadOnly = true;
            this.textBoxCode.Size = new System.Drawing.Size(252, 35);
            this.textBoxCode.TabIndex = 3;
            this.textBoxCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // folderBrowserDialogNetwork
            // 
            this.folderBrowserDialogNetwork.Description = "Επιλέξτε τη θέση αποθήκευσης των δεδομένων";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.button2.Location = new System.Drawing.Point(954, 152);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 84);
            this.button2.TabIndex = 13;
            this.button2.Text = "Πελάτες με οφειλές";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label10.Location = new System.Drawing.Point(4, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(244, 20);
            this.label10.TabIndex = 10;
            this.label10.Text = "Περιγραφή";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.DropDownWidth = 500;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(4, 47);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(244, 28);
            this.comboBox1.TabIndex = 11;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            this.comboBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox1_KeyPress);
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataSource = typeof(GkinisPetros.Product);
            // 
            // pELATEBindingSource
            // 
            this.pELATEBindingSource.DataSource = typeof(GkinisPetros.Customer);
            // 
            // productBarcodeBindingSource
            // 
            this.productBarcodeBindingSource.DataSource = typeof(GkinisPetros.ProductBarcode);
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "AP_TIMH_XON";
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = null;
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn25.HeaderText = "Τιμή Χονρδικής";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 144;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "AP_TIMH_LIAN";
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn26.HeaderText = "Τιμή Λιανικής";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Width = 130;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "AP_MORFI";
            this.dataGridViewTextBoxColumn7.HeaderText = "Μορφή";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 83;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "AP_DESCRIPTION";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn6.HeaderText = "Περιγραφή";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 110;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Περιγραφή";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            this.descriptionDataGridViewTextBoxColumn.Width = 150;
            // 
            // barcodeDataGridViewTextBoxColumn
            // 
            this.barcodeDataGridViewTextBoxColumn.DataPropertyName = "Barcode";
            this.barcodeDataGridViewTextBoxColumn.HeaderText = "Barcode";
            this.barcodeDataGridViewTextBoxColumn.Name = "barcodeDataGridViewTextBoxColumn";
            this.barcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.barcodeDataGridViewTextBoxColumn.Width = 124;
            // 
            // dedomenaFarmakouBindingSource2
            // 
            this.dedomenaFarmakouBindingSource2.DataSource = typeof(GkinisPetros.DedomenaFarmakou);
            // 
            // moneyDataGridViewTextBoxColumn
            // 
            this.moneyDataGridViewTextBoxColumn.DataPropertyName = "money";
            dataGridViewCellStyle13.Format = "C2";
            dataGridViewCellStyle13.NullValue = null;
            this.moneyDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this.moneyDataGridViewTextBoxColumn.HeaderText = "Ποσό";
            this.moneyDataGridViewTextBoxColumn.Name = "moneyDataGridViewTextBoxColumn";
            this.moneyDataGridViewTextBoxColumn.Width = 92;
            // 
            // transactionCodeDataGridViewTextBoxColumn
            // 
            this.transactionCodeDataGridViewTextBoxColumn.DataPropertyName = "transactionCode";
            this.transactionCodeDataGridViewTextBoxColumn.HeaderText = "Περιγραφή";
            this.transactionCodeDataGridViewTextBoxColumn.Name = "transactionCodeDataGridViewTextBoxColumn";
            this.transactionCodeDataGridViewTextBoxColumn.Width = 150;
            // 
            // customerTransactionDataBindingSource
            // 
            this.customerTransactionDataBindingSource.DataSource = typeof(GkinisPetros.CustomerTransactionData);
            // 
            // dedomenaFarmakouBindingSource1
            // 
            this.dedomenaFarmakouBindingSource1.DataSource = typeof(GkinisPetros.DedomenaFarmakou);
            // 
            // dedomenaFarmakouBindingSource
            // 
            this.dedomenaFarmakouBindingSource.DataSource = typeof(GkinisPetros.DedomenaFarmakou);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.PapayaWhip;
            this.ClientSize = new System.Drawing.Size(1676, 888);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.flowLayoutPanel6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.showPrintedList);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.showProdListButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Δανεικά Ημέρας";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCoupons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcodeProductBindingSource2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barcodeProductBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcodeProductBindingSource)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTransactions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pELATEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBarcodeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedomenaFarmakouBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerTransactionDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedomenaFarmakouBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedomenaFarmakouBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox barcodeTextBox;
        private System.Windows.Forms.TextBox uniqueBarcodeTextBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource pELATEBindingSource;
        private System.Windows.Forms.BindingSource productBindingSource;
        private System.Windows.Forms.DataGridView productDataGridView;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem αρχείοToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem αποθήκευσηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem αποθήκευσηΩςToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem έξοδοςToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ρυθμίσειςToolStripMenuItem;
        private System.Windows.Forms.Button AddToListButton;
        private System.Windows.Forms.BindingSource barcodeProductBindingSource;
        private System.Windows.Forms.DataGridView dataGridViewCoupons;
        private System.Windows.Forms.BindingSource barcodeProductBindingSource1;
        private System.Windows.Forms.BindingSource productBarcodeBindingSource;
        private System.Windows.Forms.ToolStripMenuItem φόρτωσηΕφεδρικούΑρχείουToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem φόρτωσηΔεδομένωνToolStripMenuItem;
        private System.Windows.Forms.ToolTip barcodeErrorTooltip;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button expirationButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button autoExpirationButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolTip priceErrorToolTip;
        private System.Windows.Forms.ToolTip uniqueBarcodeToolTip;
        private System.Windows.Forms.Button showProdListButton;
        private System.Windows.Forms.BindingSource barcodeProductBindingSource2;
        private System.Windows.Forms.BindingSource dedomenaFarmakouBindingSource;
        private System.Windows.Forms.BindingSource dedomenaFarmakouBindingSource1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.BindingSource dedomenaFarmakouBindingSource2;
        private System.Windows.Forms.Label sumLabel;
        private System.Windows.Forms.TextBox sumTextBoxCoupons;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolTip dateErrorTooltip;
        private System.Windows.Forms.TextBox textBoxDelete;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxCount;
        private System.Windows.Forms.Button showPrintedList;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxCustomer;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.BindingSource tBindingSource;
        private System.Windows.Forms.BindingSource customerTransactionDataBindingSource;
        private System.Windows.Forms.DataGridView dataGridViewTransactions;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox sumTextBoxTransactions;
        private System.Windows.Forms.DataGridViewTextBoxColumn moneyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transactionCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Delete;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxSumTotal;
        private System.Windows.Forms.ToolStripMenuItem εκκίνησηΕκτύπωσηςToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem εκτύπωσηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxCode;
        private System.Windows.Forms.ListBox listBoxPhones;
        private System.Windows.Forms.ToolStripMenuItem επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ενημέρωσηΠελατώνToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogNetwork;
        private System.Windows.Forms.ToolStripMenuItem ξεκλείδωμαΠελάτηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupΣεΦάκελοToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem επαναφοράΑπόBackupToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Morfi;
        private System.Windows.Forms.DataGridViewTextBoxColumn UniqueBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpirationDate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurchaseDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn barcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
    }
}


﻿namespace GkinisPetros
{
    partial class CustomerList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pECODEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pEEPWNYMIADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pEONOMADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PE_AMKA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pEPHONEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pEKINHTODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.εκτύπωσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerTransactionDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerBindingSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customerTransactionDataBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.PapayaWhip;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pECODEDataGridViewTextBoxColumn,
            this.pEEPWNYMIADataGridViewTextBoxColumn,
            this.pEONOMADataGridViewTextBoxColumn,
            this.PE_AMKA,
            this.pEPHONEDataGridViewTextBoxColumn,
            this.pEKINHTODataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.customerBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LimeGreen;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 24);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1376, 519);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // pECODEDataGridViewTextBoxColumn
            // 
            this.pECODEDataGridViewTextBoxColumn.DataPropertyName = "PE_CODE";
            this.pECODEDataGridViewTextBoxColumn.HeaderText = "ΚΩΔΙΚΟΣ";
            this.pECODEDataGridViewTextBoxColumn.Name = "pECODEDataGridViewTextBoxColumn";
            this.pECODEDataGridViewTextBoxColumn.ReadOnly = true;
            this.pECODEDataGridViewTextBoxColumn.Width = 136;
            // 
            // pEEPWNYMIADataGridViewTextBoxColumn
            // 
            this.pEEPWNYMIADataGridViewTextBoxColumn.DataPropertyName = "PE_EPWNYMIA";
            this.pEEPWNYMIADataGridViewTextBoxColumn.HeaderText = "ΕΠΩΝΥΜΟ";
            this.pEEPWNYMIADataGridViewTextBoxColumn.Name = "pEEPWNYMIADataGridViewTextBoxColumn";
            this.pEEPWNYMIADataGridViewTextBoxColumn.ReadOnly = true;
            this.pEEPWNYMIADataGridViewTextBoxColumn.Width = 153;
            // 
            // pEONOMADataGridViewTextBoxColumn
            // 
            this.pEONOMADataGridViewTextBoxColumn.DataPropertyName = "PE_ONOMA";
            this.pEONOMADataGridViewTextBoxColumn.HeaderText = "ΟΝΟΜΑ";
            this.pEONOMADataGridViewTextBoxColumn.Name = "pEONOMADataGridViewTextBoxColumn";
            this.pEONOMADataGridViewTextBoxColumn.ReadOnly = true;
            this.pEONOMADataGridViewTextBoxColumn.Width = 121;
            // 
            // PE_AMKA
            // 
            this.PE_AMKA.DataPropertyName = "PE_AMKA";
            this.PE_AMKA.HeaderText = "ΑΜΚΑ";
            this.PE_AMKA.Name = "PE_AMKA";
            this.PE_AMKA.ReadOnly = true;
            this.PE_AMKA.Width = 101;
            // 
            // pEPHONEDataGridViewTextBoxColumn
            // 
            this.pEPHONEDataGridViewTextBoxColumn.DataPropertyName = "PE_PHONE";
            this.pEPHONEDataGridViewTextBoxColumn.HeaderText = "ΤΗΛ";
            this.pEPHONEDataGridViewTextBoxColumn.Name = "pEPHONEDataGridViewTextBoxColumn";
            this.pEPHONEDataGridViewTextBoxColumn.ReadOnly = true;
            this.pEPHONEDataGridViewTextBoxColumn.Width = 82;
            // 
            // pEKINHTODataGridViewTextBoxColumn
            // 
            this.pEKINHTODataGridViewTextBoxColumn.DataPropertyName = "PE_KINHTO";
            this.pEKINHTODataGridViewTextBoxColumn.HeaderText = "ΚΙΝΗΤΟ";
            this.pEKINHTODataGridViewTextBoxColumn.Name = "pEKINHTODataGridViewTextBoxColumn";
            this.pEKINHTODataGridViewTextBoxColumn.ReadOnly = true;
            this.pEKINHTODataGridViewTextBoxColumn.Width = 121;
            // 
            // customerBindingSource
            // 
            this.customerBindingSource.DataSource = typeof(GkinisPetros.Customer);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.εκτύπωσηToolStripMenuItem,
            this.εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1376, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // εκτύπωσηToolStripMenuItem
            // 
            this.εκτύπωσηToolStripMenuItem.Name = "εκτύπωσηToolStripMenuItem";
            this.εκτύπωσηToolStripMenuItem.Size = new System.Drawing.Size(126, 20);
            this.εκτύπωσηToolStripMenuItem.Text = "Εκτύπωση Άχρωμη";
            this.εκτύπωσηToolStripMenuItem.Click += new System.EventHandler(this.εκτύπωσηToolStripMenuItem_Click);
            // 
            // εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem
            // 
            this.εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem.Name = "εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem";
            this.εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem.Size = new System.Drawing.Size(164, 20);
            this.εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem.Text = "Εκτύπωση Όπως Φαίνεται";
            this.εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem.Click += new System.EventHandler(this.εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem_Click);
            // 
            // customerTransactionDataBindingSource
            // 
            this.customerTransactionDataBindingSource.DataSource = typeof(GkinisPetros.CustomerTransactionData);
            // 
            // CustomerList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1376, 543);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "CustomerList";
            this.ShowIcon = false;
            this.Text = "Λίστα Πελατών με Οφειλές";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CustomerList_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customerBindingSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customerTransactionDataBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource customerBindingSource;
        private System.Windows.Forms.BindingSource customerTransactionDataBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn pECODEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pEEPWNYMIADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pEONOMADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PE_AMKA;
        private System.Windows.Forms.DataGridViewTextBoxColumn pEPHONEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pEKINHTODataGridViewTextBoxColumn;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem εκτύπωσηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem;
    }
}
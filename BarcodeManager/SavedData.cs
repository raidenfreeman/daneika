﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GkinisPetros
{
    [ProtoBuf.ProtoContract]
    public class SavedData
    {
        [ProtoBuf.ProtoMember(1)]
        public List<DedomenaFarmakou> listDedomenaFarmakon;
        [ProtoBuf.ProtoMember(2)]
        public List<CustomerTransactionData> listTransactions;

        private SavedData()
        {

        }
        public SavedData(List<DedomenaFarmakou> farmaka, List<CustomerTransactionData> transactions)
        {
            listDedomenaFarmakon = farmaka;
            listTransactions = transactions;
        }
    }
}

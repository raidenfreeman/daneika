﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
//using DSOFile;

namespace GkinisPetros
{
    public partial class MainForm : Form
    {
        const string fileMutexSuffix = ".lock";
        public const string MainPrintedSaveFile = "printed.bin";
        public string currentCustomerCode = "0";
        public string lockedCustomerCode = "";
        public Customer currentlySelectedCustomer;
        const int tooltipTimer = 4000;
        private FarnetDataContext FarnetDataContext1 = new FarnetDataContext();
        public string CustomerDataDirectory;
        const string DSOPropertyNameDate = "CustomDatePropertyDaneikaImeras";

        bool barcodeFound;

        public MainForm()
        {
            InitializeComponent();
            if (Properties.Settings.Default.Installed == false)
            {
                InstallationForm n = new InstallationForm();
                n.ShowDialog();
                if (Properties.Settings.Default.DBUsername == "PETROS")
                {
                    string defaultDirectory = @"C:\Δεδομένα Δανεικών Ημέρας";
                    if (!Directory.Exists(defaultDirectory))
                        Directory.CreateDirectory(defaultDirectory);
                    UpdateCustomerDataDirectory(defaultDirectory);
                }
                else
                    AskForNewDataDirectory();
            }
            if (Properties.Settings.Default.SharedSaveFolder != null && Properties.Settings.Default.SharedSaveFolder != "")
            {
                if (Directory.Exists(Properties.Settings.Default.SharedSaveFolder))
                    UpdateCustomerDataDirectory(Properties.Settings.Default.SharedSaveFolder);
                else
                {
                    MessageBox.Show("Η θέση αποθήκευσης δεδομένων των πελατών δεν είναι προσβάσιμη. Επιλέξτε τη νέα θέση των αρχείων δεδομένων.", "Σφάλμα Αρχείων Δεδομένων", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AskForNewDataDirectory();
                }
            }
            else
                UpdateCustomerDataDirectory(Directory.GetCurrentDirectory());

            productList = new List<DedomenaFarmakou>();
            printedList = new List<DedomenaFarmakou>();
            customerMetadataList = new List<CustomerMetadata>();
            if (Properties.Settings.Default.DBConnString != "")
                FarnetDataContext1 = new FarnetDataContext(Properties.Settings.Default.DBConnString);
            this.Size = Properties.Settings.Default.WindowSize;
            this.Location = Properties.Settings.Default.WindowLocation;
            this.WindowState = Properties.Settings.Default.WindowState;
            dataGridViewTransactions.DataSource = new SortableBindingList<CustomerTransactionData>();
            comboBox1.DataSource = FarnetDataContext1.Products.OrderBy(x => x.AP_DESCRIPTION).ThenBy(x => x.AP_MORFI);
            comboBox1.DisplayMember = "FullDescription";
        }

        const string notFoundError = "Το Barcode δεν βρέθηκε.";
        const string wrongLengthError = "Το Barcode πρέπει να έχει 12 ψηφία.";
        const string invalidPriceError = "Η τιμή πρέπει μπορεί να περιέχει μόνο αριθμούς.";
        const string invalidBarcodeError = "Το Barcode μπορεί να περιέχει μόνο αριθμούς.";
        const string barcodeDuplicateError = "Το μοναδικό Barcode υπάρχει ήδη.";
        const string invalidDateError = "Εσφαλμένη Ημερομηνία";
        Product tempProduct;
        public List<DedomenaFarmakou> productList;
        public List<DedomenaFarmakou> printedList;

        public List<CustomerMetadata> customerMetadataList;

        private void Form1_Load(object sender, EventArgs e)
        {
            Customer lastCustomerEdited = null;
            var myFile = new DirectoryInfo(CustomerDataDirectory).GetFiles().OrderByDescending(f => f.LastWriteTime).FirstOrDefault();
            if (myFile != null)
                lastCustomerEdited = FarnetDataContext1.Customers.Where(x => x.PE_CODE == Path.GetFileNameWithoutExtension(myFile.ToString())).FirstOrDefault();
            LoadListFromSaves();
            try
            {
                comboBoxCustomer.DataSource = FarnetDataContext1.Customers.OrderBy(x => x.PE_EPWNYMIA);
            }
            catch (Exception)
            {
                MessageBox.Show("Σφάλμα στο connection string");
                Application.Exit();
                //comboBoxCustomer.DataSource = new FarnetDataContext(@"Data Source=(local)\CSASQL;Initial Catalog=Farnet_2013;Integrated Security=True").Customers.OrderBy(x => x.PE_EPWNYMIA);
            }
            comboBoxCustomer.DisplayMember = "FullName";
            LoadMetadata();
            if (lastCustomerEdited != null)
                LoadCustomer(lastCustomerEdited);
            comboBoxCustomer.Focus();
        }

        private void έξοδοςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckTexBoxes())
            {
                barcodeTextBox.Focus();
                AddToSavedList();
                SaveToFile();
                ClearTextBoxes();
            }
        }

        private void ClearTextBoxes()
        {
            barcodeTextBox.Clear();
            uniqueBarcodeTextBox.Clear();
            priceTextBox.Clear();
            maskedTextBox1.Clear();
        }

        private void AddToSavedList()
        {
            DateTime? date;
            if (maskedTextBox1.TextLength == 10)
            {
                DateTime dateNotNull;
                DateTime.TryParse(maskedTextBox1.Text, out dateNotNull);
                if (dateNotNull == DateTime.MinValue)
                {
                    //MessageBox.Show("Η ημερομηνία " + maskedTextBox1.Text + " δεν είναι έγκυρη.\nΘα τεθεί κενή.");
                    date = null;
                }
                else
                    date = dateNotNull;
            }
            else
                date = null;
            if (productList == null)
                productList = new List<DedomenaFarmakou>();
            productList.Add(new DedomenaFarmakou(tempProduct, barcodeTextBox.Text, uniqueBarcodeTextBox.Text, date, priceTextBox.Text));
            UpdateDataGridViewDataSource(dataGridViewCoupons, productList);
        }

        void UpdateDataGridViewDataSource<T>(DataGridView dg, IEnumerable<T> list)
        {
            if (list == null)
            {
                dg.DataSource = new SortableBindingList<T>();
                return;
            }
            var source = list.ToList();
            source.Reverse();
            dg.DataSource = new SortableBindingList<T>(source);
        }

        private bool CheckTexBoxes()
        {
            var a = PriceValidity();
            var b = UniqueBarcodeValidity();
            var c = BarcodeValidity();
            return a && b && c;
        }

        bool PriceValidity()
        {
            decimal k;
            if (!IsTextBoxDecimal(priceTextBox, out k))
            {
                priceTextBox.SelectAll();
                priceErrorToolTip.Show(invalidPriceError, priceTextBox, 0, 20, tooltipTimer);
                return false;
            }
            return true;
        }

        bool UniqueBarcodeValidity()
        {/*
            string errorMessage;
            if (!IsUniqueBarcode(uniqueBarcodeTextBox.Text, barcodeTextBox.Text, out errorMessage))
            {
                uniqueBarcodeTextBox.SelectAll();
                uniqueBarcodeToolTip.Show(errorMessage, uniqueBarcodeTextBox, 0, 20, tooltipTimer);
                return false;
            }
            if (uniqueBarcodeTextBox.Text.Length != maxUniqueBarcodeLength)
            {
                uniqueBarcodeTextBox.SelectAll();
                uniqueBarcodeToolTip.Show(wrongLengthError, uniqueBarcodeTextBox, 0, 20, tooltipTimer);
                return false;
            }*/
            return true;
        }

        bool BarcodeValidity()
        {
            if (!barcodeFound)
            {
                barcodeTextBox.SelectAll();
                barcodeErrorTooltip.Show(notFoundError, barcodeTextBox, 0, 20, tooltipTimer);
                return false;
            }
            return true;
        }

        private bool IsTextBoxDecimal(TextBox t, out decimal g)
        {
            return decimal.TryParse(t.Text.Replace(".", ","), out g);
        }

        private void αποθήκευσηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveToFile();
        }

        void SaveToFile()
        {
            string customerFile = CustomerDataDirectory + currentCustomerCode + ".bin";
            string mantissa = "temp";
            int i = 0;
            while (true)
            {
                if (File.Exists(customerFile + mantissa))
                {
                    mantissa += i.ToString();
                    i++;
                }
                else
                    break;
            }
            try
            {
                using (var a = File.Create(customerFile + mantissa))
                {
                    var bb = new List<CustomerTransactionData>();
                    if (dataGridViewTransactions.RowCount > 0 && dataGridViewTransactions.Rows[0] != null && dataGridViewTransactions.Rows[0].Cells[0] != null && dataGridViewTransactions.Rows[0].Cells[0].Value != null)
                        bb = ((SortableBindingList<CustomerTransactionData>)dataGridViewTransactions.DataSource).ToList();
                    SavedData data = new SavedData(productList, bb);
                    ProtoBuf.Serializer.Serialize(a, data);
                }
                File.Delete(customerFile);
                File.Copy(customerFile + mantissa, customerFile);
                File.Delete(customerFile + mantissa);
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            SaveMetadata();
        }

        /*void SaveMetadata(string customerFile)
        {
            DateTime date = DateTime.MinValue;
            if (productList != null && productList.Count() > 0)
            {
                var earliestExpiringProduct = productList.OrderBy(x => x.ExpirationDate).FirstOrDefault();
                if (earliestExpiringProduct.ExpirationDate != null)
                    date = earliestExpiringProduct.ExpirationDate.Value;
            }
            OleDocumentProperties odp = new OleDocumentProperties();
            if (!File.Exists(customerFile))
            {
                var b = File.Create(customerFile);
                b.Close();
            }
            odp.Open(customerFile);
            foreach (CustomProperty p in odp.CustomProperties)
                if (p.Name == DSOPropertyNameDate)
                    p.set_Value(date);
            if (odp.CustomProperties.Count == 0)
                odp.CustomProperties.Add(DSOPropertyNameDate, date);
            odp.Save();
            odp.Close(true);
        }*/

        void SaveMetadata()
        {
            string metadataDirectory = Path.Combine(CustomerDataDirectory, @"Metadata\");
            if (productList != null && productList.Count() > 0)
            {
                DateTime date;
                var earliestExpiringProduct = productList.OrderBy(x => x.ExpirationDate).FirstOrDefault();
                if (earliestExpiringProduct.ExpirationDate != null)
                {
                    date = earliestExpiringProduct.ExpirationDate.Value;
                    if (!Directory.Exists(metadataDirectory))
                        Directory.CreateDirectory(metadataDirectory);
                    customerMetadataList.RemoveAll(x => x.code == currentCustomerCode);
                    customerMetadataList.Add(new CustomerMetadata(currentCustomerCode, date));
                    string metadataFileName = currentCustomerCode + '_' + date.ToString("yyyy-dd-MM");
                    string metadataFilePath = metadataDirectory + metadataFileName;
                    string[] foundFiles = Directory.GetFiles(metadataDirectory, currentCustomerCode + '_' + '*');

                    int count = foundFiles.Count();
                    if (count > 1)
                        MessageBox.Show("Σημαντικό σφάλμα μεταδεδομένων. Επικοινωνήστε με τον κατασκευαστή το προγράμματος");
                    if (count == 1)
                        File.Delete(foundFiles[0]);
                    try
                    {
                        var a = File.Create(metadataFilePath);
                        a.Close();
                    }
                    catch (UnauthorizedAccessException)
                    {
                        MessageBox.Show("Δεν αποθηκεύτηκαν τα μεταδεδομένα, οι προσερχείς λήξεις μπορεί να είναι λάθος.");
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("Δεν αποθηκεύτηκαν τα μεταδεδομένα, οι προσερχείς λήξεις μπορεί να είναι λάθος.");
                    }
                }
                else
                {
                    if (Directory.Exists(metadataDirectory))
                    {
                        string[] foundFiles = Directory.GetFiles(metadataDirectory, currentCustomerCode + '_' + '*');
                        if (foundFiles.Count() > 0)
                            foreach (var f in foundFiles)
                                File.Delete(f);
                    }
                }
            }
            else
            {
                if (Directory.Exists(metadataDirectory))
                {
                    string[] foundFiles = Directory.GetFiles(metadataDirectory, currentCustomerCode + '_' + '*');
                    if (foundFiles.Count() > 0)
                        foreach (var f in foundFiles)
                            File.Delete(f);
                }
            }
        }

        /*void UpdateMainSaveFile()
        {
            DateTime? date = null;
            if (productList != null && productList.Count() > 0)
            {
                var earliestExpiringProduct = productList.OrderBy(x => x.ExpirationDate).FirstOrDefault();
                if (earliestExpiringProduct.ExpirationDate != null)
                    date = earliestExpiringProduct.ExpirationDate;
            }
            var customerToUpdate = new CustomerMetadata(currentlySelectedCustomer.FullName, currentlySelectedCustomer.PE_CODE, totalSum, date);
            var existingCustomer = customerMetadataList.FirstOrDefault(x => x.code == customerToUpdate.code);
            if (existingCustomer != null)
                customerMetadataList.Remove(existingCustomer);
            customerMetadataList.Insert(0, customerToUpdate);
            string mantissa = "temp";
            int i = 0;
            while (true)
            {
                if (File.Exists(MainSaveFile + mantissa))
                {
                    mantissa += i.ToString();
                    i++;
                }
                else
                    break;
            }
            try
            {
                using (var a = File.Create(MainSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize(a, customerMetadataList);
                }
                File.Delete(MainSaveFile);
                File.Copy(MainSaveFile + mantissa, MainSaveFile);
                File.Delete(MainSaveFile + mantissa);
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (DirectoryNotFoundException e)
            {
                MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException e)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }*/

        bool StorePrinted()
        {
            string mantissa = DateTime.Today.ToString("_dd-MM-yyyy");
            int i = 0;
            while (true)
            {
                if (File.Exists(MainPrintedSaveFile + mantissa))
                {
                    mantissa += "(" + i.ToString() + ")";
                    i++;
                }
                else
                    break;
            }
            try
            {
                using (var a = File.Create(MainPrintedSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<DedomenaFarmakou>>(a, productList);
                    return true;
                }
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (IOException)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Αποθήκευσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void φόρτωσηΔεδομένωνToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadListFromSaves();
        }

        private void LoadListFromSaves()
        {
            productList = new List<DedomenaFarmakou>();
            string saveFile = CustomerDataDirectory + currentCustomerCode + ".bin";
            if (!File.Exists(saveFile))
            {
                dataGridViewCoupons.DataSource = null;
                dataGridViewCoupons.Rows.Clear();
                dataGridViewCoupons.DataSource = new SortableBindingList<DedomenaFarmakou>();
                dataGridViewTransactions.DataSource = new SortableBindingList<CustomerTransactionData>();
                dataGridViewTransactions.Rows.Clear();
                return;
            }
            try
            {
                using (var file = File.OpenRead(saveFile))
                {
                    SavedData s = ProtoBuf.Serializer.Deserialize<SavedData>(file);
                    productList = s.listDedomenaFarmakon;
                    file.Close();
                    UpdateDataGridViewDataSource(dataGridViewCoupons, productList);
                    UpdateDataGridViewDataSource(dataGridViewTransactions, s.listTransactions);
                }

            }
            catch (ArgumentException)
            {
                MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void LoadMetadata()
        {
            string metadataDirectory = Path.Combine(CustomerDataDirectory, @"Metadata\");
            if (Directory.Exists(metadataDirectory))
            {
                customerMetadataList = new List<CustomerMetadata>();
                string[] fileList = Directory.GetFiles(metadataDirectory);
                foreach (var file in fileList)
                {
                    CustomerMetadata m = FileAnalysis(Path.GetFileName(file));
                    if (m != null)
                        customerMetadataList.Add(m);
                }
                var today = DateTime.Today;
                var endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
                FindExpiredProducts(endOfMonth, customerMetadataList);
            }
            /*if (File.Exists(filepath))
            {
                DateTime? date = null;
                OleDocumentProperties odp = new OleDocumentProperties();
                odp.Open(filepath);
                foreach (CustomProperty p in odp.CustomProperties)
                    if (p.Name == DSOPropertyNameDate)
                        date = p.get_Value();
                odp.Close();
                if (date != null)
                    MessageBox.Show(date.Value.ToShortDateString());
            }*/
        }

        CustomerMetadata FileAnalysis(string file)
        {
            if (file == null || file == "")
                return null;
            string[] result = file.Split('_');
            //invalid string
            if (result.Count() != 2)
                return null;
            DateTime d;
            if (DateTime.TryParseExact(result[1], "yyyy-dd-MM", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out d))
                if (DateTime.MinValue == d)
                    return new CustomerMetadata(result[0], null);
                else
                    return new CustomerMetadata(result[0], d);
            else
                return null;
        }

        CustomerMetadata FileAnalysisNotMetadata(string file)
        {
            if (file == null || file == "")
                return null;
            string[] result = file.Split('.');
            //invalid string
            if (result.Count() != 2 || result[1] == "lock")
                return null;
            return new CustomerMetadata(result[0], null);
        }
        /*void LoadMainSaveFile()
        {
            if (File.Exists(MainSaveFile))
            {
                try
                {
                    using (var file = File.OpenRead(MainSaveFile))
                    {
                        customerMetadataList = ProtoBuf.Serializer.Deserialize<List<CustomerMetadata>>(file);
                        file.Close();
                        if (customerMetadataList.FirstOrDefault() != null)
                        {
                            try
                            {
                                var c = FarnetDataContext1?.Customers?.Where(x => x.PE_CODE == customerMetadataList.First().code).FirstOrDefault();
                                if (c != null)
                                    LoadCustomer(c);
                            }
                            catch (System.Data.SqlClient.SqlException)
                            {
                                MessageBox.Show("hello");
                            }
                            //textBoxSumOfAllCustomers.Text = customerMetadataList.Sum(x => x.totalOwed).ToString("C");
                            var today = DateTime.Today;
                            var endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
                            FindExpiredProducts(endOfMonth, customerMetadataList);
                        }
                    }
                }
                catch (ArgumentException e)
                {
                    MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (DirectoryNotFoundException e)
                {
                    MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (UnauthorizedAccessException e)
                {
                    MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (IOException e)
                {
                    MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }*/

        private void εκκίνησηΕκτύπωσηςToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            XLSXPrinting.DoYourThing(productList);
            if (StorePrinted())
            {
                productList.Clear();
                UpdateDataGridViewDataSource(dataGridViewCoupons, productList);
                SaveToFile();
            }
            else
            {
                if (MessageBox.Show("Παρουσιάστηκε σφάλμα κατά την αποθήκευση των εκτυπωμένων κουπονιών.\nΕάν θέλετε να ΧΑΘΟΥΝ τα εκτυπωμένα κουπόνια πατήστε άκυρο.\nΑλλιώς πατήστε ξανά κάποιο απ' τα κουμπιά εκτύπωσης", "Σφάλμα κατά την αποθήκευση εκτυπωμένων", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    MessageBox.Show("Τα κουπόνια δεν αποθηκεύτηκαν.");
                    productList.Clear();
                    UpdateDataGridViewDataSource(dataGridViewCoupons, productList);
                }
            }
            string labelFilepath = @"couponsAutoOLD.lbl";
            FileInfo labelFile = new FileInfo(labelFilepath);
            if (labelFile.Exists)
            {
                try
                {
                    System.Diagnostics.Process.Start(labelFilepath);
                }
                catch (System.AccessViolationException)
                {
                    MessageBox.Show("Δεν έχετε δικαιώματα για να ανοίξετε το αρχείο ετικετών: " + labelFilepath + ". Προσπαθήστε να εκτελέσετε το πρόγραμμα ως διαχειριστής, ή να αλλάξετε τα δικαιώματα του αρχείου.", "Σφάλμα Εκκίνησης Εκτύπωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Tο αρχείο ετικετών : \"" + labelFilepath + "\" δεν βρέθηκε.", "Σφάλμα Εκκίνησης Εκτύπωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void FindExpired(DateTime comparison)
        {
            comparison = comparison.Date;
            if (productList == null)
                return;
            var q = productList.Where(x => x.ExpirationDate <= comparison).ToList();
            UpdateDataGridViewDataSource(dataGridViewCoupons, q);
        }

        private void FindExpiredProducts(DateTime comparison, List<CustomerMetadata> customerMetadataList)
        {
            comparison = comparison.Date;
            var q = customerMetadataList.Where(x => x.earliestExpiration <= comparison).Select(x => x.code);
            if (q != null && q.Count() > 0)
            {
                string sumString = "";
                foreach (string s in q)
                {
                    var tempCustomer = FarnetDataContext1.Customers.Where(x => x.PE_CODE == s).FirstOrDefault();
                    if (tempCustomer != null)
                        sumString += "\n  " + tempCustomer.PE_EPWNYMIA + " " + tempCustomer.PE_ONOMA + "\t\t" + s;
                }
                MessageBox.Show("Αυτό το μήνα λήγουν οφειλές στους ακόλουθους πελάτες:\n" + sumString, "Λήγουν Μέχρι " + comparison.ToShortDateString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FindExpired(dateTimePicker2.Value);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var today = DateTime.Today;
            int month = today.Month;
            int year = today.Year;
            int lastDayOfMonth = DateTime.DaysInMonth(year, month);
            var endOfMonth = new DateTime(year, month, lastDayOfMonth);
            FindExpired(endOfMonth);
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if ((senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn) || (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn) && e.RowIndex >= 0)
            {
                productList.Remove(((DedomenaFarmakou)senderGrid.Rows[e.RowIndex].DataBoundItem));
                senderGrid.Rows.RemoveAt(e.RowIndex);
                SaveToFile();
                textBoxSumTotal.Text = totalSum.ToString("C");
            }
        }

        public bool IsUniqueBarcode(string uniqueBarcode, string barcode, out string errorMessage)
        {
            if (uniqueBarcode != string.Empty && !IsDigitsOnly(uniqueBarcode))
            {
                errorMessage = invalidBarcodeError;
                return false;
            }
            if (productList != null && productList.Any(x => x.Barcode == barcode && x.UniqueBarcode == uniqueBarcode))
            {
                errorMessage = barcodeDuplicateError;
                return false;
            }
            errorMessage = "";
            return true;
        }
        bool IsDigitsOnly(string str)

        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        private void showProdListButton_Click(object sender, EventArgs e)
        {
            UpdateDataGridViewDataSource(dataGridViewCoupons, productList);
        }

        private void LoadPrinted()
        {
            DirectoryInfo taskDirectory = new DirectoryInfo(Environment.CurrentDirectory);
            FileInfo[] taskFiles = taskDirectory.GetFiles(MainPrintedSaveFile + "*");
            if (printedList == null)
                printedList = new List<DedomenaFarmakou>();
            else
                printedList.Clear();
            foreach (FileInfo fi in taskFiles)
            {
                try
                {
                    using (var file = fi.OpenRead())
                    {
                        printedList.AddRange(ProtoBuf.Serializer.Deserialize<List<DedomenaFarmakou>>(file));
                        file.Close();
                    }
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Δεν βρέθηκε το αρχείο αποθηκευμένων δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (DirectoryNotFoundException)
                {
                    MessageBox.Show("Σφάλμα στο όνομα φακέλου του αρχείου δεδομένων.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("Δεν έχετε δικαιώματα ανάγνωσης του αρχείου αποθηκευμένων δεδομένων.\nΔοκιμάστε να αλλάξετε θέση στο φάκελο του προγράμματος, ή να το εκτελέσετε ως διαχειριστής.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (IOException)
                {
                    MessageBox.Show("Σφάλμα κατά το άνοιγμα του αρχείου.\nΠροσπαθήστε ξανά αργότερα.", "Σφάλμα Φόρτωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void priceTextBox_Leave(object sender, EventArgs e)
        {
            priceTextBox.Text = priceTextBox.Text.Replace(".", ",");
        }

        private SortOrder getSortOrder(int columnIndex)
        {
            var direction = dataGridViewCoupons.Columns[columnIndex].HeaderCell.SortGlyphDirection;
            if (direction == SortOrder.None ||
                direction == SortOrder.Descending)
            {
                direction = SortOrder.Ascending;
                return SortOrder.Ascending;
            }
            else
            {
                direction = SortOrder.Descending;
                return SortOrder.Descending;
            }
        }

        private void αλλαγήΒάσηςΔεδομένωνFarmakonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectDB n = new SelectDB();
            DialogResult res = n.ShowDialog();
            //string s = System.Configuration.ConfigurationManager.ConnectionStrings["portalConnectionString-Prod"].ConnectionString;
            if (res == DialogResult.OK)
                FarnetDataContext1 = new FarnetDataContext(Properties.Settings.Default.DBConnString);
            //FarnetDataContext1.Connection.ConnectionString = @"Data Source=(local)\CSASQL;Initial Catalog=Farnet_2015;Integrated Security=True";
            //MessageBox.Show(FarnetDataContext1.Connection.ConnectionString);
            /*
            string newLocation = "";
            string defaultDirectory = @"C:\FarmakoNet\SQLData\";
            if (Directory.Exists(defaultDirectory))
                openFileDialog1.InitialDirectory = defaultDirectory;
            var result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
                newLocation = openFileDialog1.FileName;
            if (newLocation == "")
                return;
            else
            {
                Properties.Settings.Default.DBName = newLocation;
                Properties.Settings.Default.Save();
            }*/
        }

        private void dataGridView2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewCoupons.Columns[e.ColumnIndex].Name == "Price")
                sumTextBoxCoupons.Text = CellSum(e.ColumnIndex, dataGridViewCoupons).ToString("C");
        }

        private decimal CellSum(int column, DataGridView dg)
        {
            decimal sum = 0;
            for (int i = 0; i < dg.Rows.Count; ++i)
            {
                decimal d = 0;
                var a = dg.Rows[i].Cells[column].Value;
                if (a != null)
                {
                    decimal.TryParse(a.ToString(), out d);
                    sum += d;
                }
            }
            return sum;
        }

        decimal sumCoupons
        {
            get
            {
                if (dataGridViewCoupons.DataSource != null && dataGridViewCoupons.RowCount > 0)
                {
                    var a = dataGridViewCoupons.Columns["Price"];
                    if (a != null)
                    {
                        return CellSum(a.Index, dataGridViewCoupons);
                    }
                }
                return 0;
            }
        }
        private void dataGridView2_DataSourceChanged(object sender, EventArgs e)
        {
            sumTextBoxCoupons.Text = sumCoupons.ToString("C");
        }

        private void sumTextBox_TextChanged(object sender, EventArgs e)
        {
            textBoxSumTotal.Text = totalSum.ToString("C");
        }

        private void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (maskedTextBox1.Text.Length == 10)
            {
                if (CheckDate())
                    SelectNextControl(maskedTextBox1, true, true, true, true);
                else
                {
                    maskedTextBox1.SelectAll();
                    dateErrorTooltip.Show(invalidDateError, maskedTextBox1, 0, 20, tooltipTimer);
                }
            }

        }

        private bool CheckDate()
        {
            DateTime date;
            DateTime.TryParse(maskedTextBox1.Text, out date);
            if (date == DateTime.MinValue)
                return false;
            else
                return true;
        }

        private void εκκίνησηΕκτύπωσηςLP2824PLUSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            XLSXPrinting.DoYourThing(productList, " €");
            string labelFilepath = @"couponsAutoPLUS.lbl";
            FileInfo labelFile = new FileInfo(labelFilepath);
            if (labelFile.Exists)
            {
                try
                {
                    System.Diagnostics.Process.Start(labelFilepath);
                }
                catch (System.AccessViolationException)
                {
                    MessageBox.Show("Δεν έχετε δικαιώματα για να ανοίξετε το αρχείο ετικετών¨: " + labelFilepath + ". Προσπαθήστε να εκτελέσετε το πρόγραμμα ως διαχειριστής, ή να αλλάξετε τα δικαιώματα του αρχείου.", "Σφάλμα Εκκίνησης Εκτύπωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Tο αρχείο ετικετών : \"" + labelFilepath + "\" δεν βρέθηκε.", "Σφάλμα Εκκίνησης Εκτύπωσης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (CheckTexBoxes())
                {
                    barcodeTextBox.Focus();
                    AddToSavedList();
                    SaveToFile();
                    ClearTextBoxes();
                }
            }
        }

        private void priceTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (CheckTexBoxes())
                {
                    barcodeTextBox.Focus();
                    AddToSavedList();
                    SaveToFile();
                    ClearTextBoxes();
                }
            }
        }

        private void textBoxDelete_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && dataGridViewCoupons.DataSource != null)
            {
                var refToDatasource = ((SortableBindingList<DedomenaFarmakou>)dataGridViewCoupons.DataSource).ToList();
                int productIndex = refToDatasource.FindIndex(x => x.UniqueBarcode == textBoxDelete.Text);
                //int productIndex = productList.FindIndex(x => x.Barcode == textBoxDelete.Text);
                if (productIndex < 0)
                {
                    textBoxDelete.SelectAll();
                    MessageBox.Show(this, "Tο barcode δεν βρίσκεται στη λίστα.", "Διαγραφή Barcode", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    dataGridViewCoupons.Rows.RemoveAt(productIndex);
                    productList.Remove(productList.Find(x => x.UniqueBarcode == textBoxDelete.Text));
                    SaveToFile();
                    textBoxDelete.Clear();
                }
                textBoxSumTotal.Text = totalSum.ToString("C");
            }
        }

        private void dataGridView2_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            textBoxCount.Text = dataGridViewCoupons.RowCount.ToString();
        }

        private void dataGridView2_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            textBoxCount.Text = dataGridViewCoupons.RowCount.ToString();
            if (dataGridViewCoupons.DataSource != null && dataGridViewCoupons.RowCount > 0)
            {
                var a = dataGridViewCoupons.Columns["Price"];
                if (a != null)
                {
                    sumTextBoxCoupons.Text = CellSum(a.Index, dataGridViewCoupons).ToString("C");
                }
            }
            else
                sumTextBoxCoupons.Text = 0.ToString("C");
        }

        private void uniqueBarcodeTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                maskedTextBox1.Focus();
                /*if (CheckTexBoxes())
                {
                    maskedTextBox1.Focus();
                    AddToSavedList();
                    SaveToFile();
                    ClearTextBoxes();
                }*/
            }
        }

        private void showPrintedList_Click(object sender, EventArgs e)
        {
            LoadPrinted();
            if (printedList != null)
                UpdateDataGridViewDataSource(dataGridViewCoupons, printedList);
        }

        private void dataGridView2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 5)
                {
                    DateTime date;
                    var a = dataGridViewCoupons.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    if (a != null && !DateTime.TryParse(a.ToString(), out date))
                        dataGridViewCoupons.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null;
                }
                if (dataGridViewCoupons.Columns[e.ColumnIndex].Name == "PurchaseDate")
                {
                    DateTime date;
                    var a = dataGridViewCoupons.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    if (a != null && !DateTime.TryParse(a.ToString(), out date))
                        dataGridViewCoupons.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null;
                }
                SaveToFile();
            }
        }

        private void barcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                string barcode = barcodeTextBox.Text;
                IQueryable<Product> selectByBarcodeQuery;
                //check the DB
                selectByBarcodeQuery = FarnetDataContext1.Products.Where(x => x.ProductBarcodes.Any(y => y.BRAP_AP_BARCODE == barcodeTextBox.Text));
                try
                {
                    tempProduct = selectByBarcodeQuery.FirstOrDefault();
                    productBindingSource.DataSource = selectByBarcodeQuery;
                }
                catch (System.Data.SqlClient.SqlException)
                {
                    MessageBox.Show("Δεν βρέθηκε η βάση δεδομένων.", "Σφάλμα αναζήτησης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                barcodeFound = (tempProduct != null);
                if (barcodeFound)
                {
                    priceTextBox.Text = tempProduct.AP_TIMH_LIAN.GetValueOrDefault().ToString("0.##");
                    SelectNextControl(barcodeTextBox, true, true, true, true);
                }
                else
                {
                    //try to find AP_CODE
                    selectByBarcodeQuery = FarnetDataContext1.Products.Where(x => x.AP_CODE == barcodeTextBox.Text);
                    try
                    {
                        tempProduct = selectByBarcodeQuery.FirstOrDefault();
                        productBindingSource.DataSource = selectByBarcodeQuery;
                    }
                    catch (System.Data.SqlClient.SqlException)
                    {
                        MessageBox.Show("Δεν βρέθηκε η βάση δεδομένων.", "Σφάλμα αναζήτησης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    barcodeFound = (tempProduct != null);
                    if (barcodeFound)
                    {
                        priceTextBox.Text = tempProduct.AP_TIMH_LIAN.GetValueOrDefault().ToString("0.##");
                        SelectNextControl(barcodeTextBox, true, true, true, true);
                    }
                    else
                    {
                        barcodeTextBox.SelectAll();
                        barcodeErrorTooltip.Show(notFoundError, barcodeTextBox, 0, 20, tooltipTimer);
                    }
                }
            }
        }

        private void dataGridView2_Sorted(object sender, EventArgs e)
        {
            if (dataGridViewCoupons.DataSource != null && dataGridViewCoupons.RowCount > 0)
            {
                var a = dataGridViewCoupons.Columns["Price"];
                if (a != null)
                {
                    sumTextBoxCoupons.Text = CellSum(a.Index, dataGridViewCoupons).ToString("C");
                }
            }
            else
                sumTextBoxCoupons.Text = 0.ToString("C");
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.WindowState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
                Properties.Settings.Default.WindowSize = this.Size;
            else
                Properties.Settings.Default.WindowSize = this.RestoreBounds.Size;
            Properties.Settings.Default.WindowLocation = this.Location;
            Properties.Settings.Default.Save();
        }

        void KeepTryingLock(string customerCode)
        {
            while (!TryLock(customerCode))
            {
                Thread.Sleep(100);
            }
            this.BeginInvoke(new MethodInvoker(delegate { SetInputControls(true); }));
        }

        static Thread lockTrialThread;
        private void LoadCustomer(Customer customer)
        {
            if (lockTrialThread != null && lockTrialThread.IsAlive)
            {
                lockTrialThread.Abort();
            }
            //unlock the previous customer
            UnlockLastFile();
            //if it's not locked
            if (TryLock(customer.PE_CODE))
                SetInputControls(true);
            else
            {
                SetInputControls(false);
                lockTrialThread = new Thread(() => KeepTryingLock(customer.PE_CODE));
                lockTrialThread.IsBackground = true;
                lockTrialThread.Start();
            }
            currentlySelectedCustomer = customer;
            currentCustomerCode = customer.PE_CODE;
            productList = new List<DedomenaFarmakou>();
            LoadListFromSaves();
            comboBoxCustomer.Text = customer.FullName;
            List<string> l = new List<string>();
            if (customer.PE_PHONE != "")
                l.Add(customer.PE_PHONE);
            if (customer.PE_KINHTO != "")
                l.Add(customer.PE_KINHTO);

            listBoxPhones.DataSource = l;

            textBoxCode.Text = currentCustomerCode;

            ClearTextBoxes();
            productBindingSource.DataSource = null;
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                string kodikosPelati = comboBoxCustomer.Text;
                if (System.Text.RegularExpressions.Regex.IsMatch(kodikosPelati, @"^[0-9]+$"))
                {
                    //check the DB for customer codes
                    var selectByCodeQuery = FarnetDataContext1.Customers.Where(x => x.PE_CODE == kodikosPelati);
                    Customer tempCustomer = null;
                    try
                    {
                        tempCustomer = selectByCodeQuery.FirstOrDefault();
                    }
                    catch (System.Data.SqlClient.SqlException)
                    {
                        MessageBox.Show("Δεν βρέθηκε η βάση δεδομένων.", "Σφάλμα αναζήτησης", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    if (tempCustomer != null)
                    {
                        LoadCustomer(tempCustomer);
                        SelectNextControl(comboBoxCustomer, true, true, true, true);
                    }
                    else
                    {
                        barcodeTextBox.SelectAll();
                        barcodeErrorTooltip.Show("Ο κωδικός πελάτη δεν βρέθηκε.", comboBoxCustomer, 0, 20, tooltipTimer);
                    }
                }
                SelectNextControl(comboBoxCustomer, true, true, true, true);
            }
        }

        private void comboBoxCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCustomer((Customer)comboBoxCustomer.SelectedItem);
            //SelectNextControl(comboBoxCustomer, true, true, true, true);
        }

        decimal sumOfTransactions
        {
            get
            {
                if (dataGridViewTransactions.DataSource != null && dataGridViewTransactions.RowCount > 0)
                {
                    var a = dataGridViewTransactions.Columns["moneyDataGridViewTextBoxColumn"];
                    if (a != null)
                    {
                        return CellSum(a.Index, dataGridViewTransactions);
                    }
                }
                return 0;
            }
        }
        private void dataGridViewTransactions_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewTransactions.Columns[e.ColumnIndex].Name == "moneyDataGridViewTextBoxColumn")
            {
                sumTextBoxTransactions.Text = sumOfTransactions.ToString("C");
            }
        }

        decimal totalSum
        {
            get
            {
                return sumCoupons + sumOfTransactions;
            }
        }
        private void sumTextBoxTransactions_TextChanged(object sender, EventArgs e)
        {
            textBoxSumTotal.Text = totalSum.ToString("C");
        }

        private void dataGridViewTransactions_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            sumTextBoxTransactions.Text = sumOfTransactions.ToString("C");
        }

        private void dataGridViewTransactions_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            sumTextBoxTransactions.Text = sumOfTransactions.ToString("C");
        }

        private void dataGridViewTransactions_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
                SaveToFile();
        }

        private void dataGridViewTransactions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            {
                senderGrid.Rows.RemoveAt(e.RowIndex);
                SaveToFile();
                textBoxSumTotal.Text = totalSum.ToString("C");
            }
        }

        private void οφειλέςΤρέχοντοςΠελάτηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (displayedList.Count == 0)
            //  return;
            PrintingManager p = new PrintingManager((Customer)comboBoxCustomer.SelectedItem, totalSum, sumCoupons, sumOfTransactions, ((SortableBindingList<DedomenaFarmakou>)dataGridViewCoupons.DataSource).ToList(), ((SortableBindingList<CustomerTransactionData>)dataGridViewTransactions.DataSource).ToList());
            p.PrintAll();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var today = DateTime.Today;
            var endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
            FindExpiredProducts(endOfMonth, customerMetadataList);
        }

        private void επιλογήΧρήστηΒάσηςΔεδομένωνToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstallationForm n = new InstallationForm();
            n.ShowDialog();
        }

        private void ενημέρωσηΠελατώνToolStripMenuItem_Click(object sender, EventArgs e)
        {
            comboBoxCustomer.DataSource = FarnetDataContext1.Customers.OrderBy(x => x.PE_EPWNYMIA);
            comboBoxCustomer.DisplayMember = "FullName";
            MessageBox.Show("Η λίστα πελατών ενημερώθηκε", "Επιτυχία Ενημέρωσης");
        }

        private void ορισμόςΘέσηςΑποθήκευσηςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialogResult = folderBrowserDialogNetwork.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                UnlockLastFile();
                UpdateCustomerDataDirectory(folderBrowserDialogNetwork.SelectedPath);
            }
        }

        void UpdateCustomerDataDirectory(string newDirectory)
        {
            Properties.Settings.Default.SharedSaveFolder = newDirectory;
            Properties.Settings.Default.Save();
            CustomerDataDirectory = Path.Combine(newDirectory, @"DedomenaPelaton\");
            if (!Directory.Exists(CustomerDataDirectory))
                Directory.CreateDirectory(CustomerDataDirectory);
        }

        void AskForNewDataDirectory()
        {
            var dialogResult = folderBrowserDialogNetwork.ShowDialog();
            if (dialogResult == DialogResult.OK)
                UpdateCustomerDataDirectory(folderBrowserDialogNetwork.SelectedPath);
            else
            {
                UpdateCustomerDataDirectory(Directory.GetCurrentDirectory());
                MessageBox.Show("Δεν επιλέχθηκε φάκελος αποθήκευσης. Θα χρησιμοποιηθεί ο φάκελος του προγράμματος για την αποθήκευση των δεδομένων.\n\nΜπορείτε να αλλάξετε το φάκελο αποθήκευσης από το μενού ρυθμίσεων.");
            }
        }

        bool TryLock(string customerCode)
        {
            string mutexFile = CustomerDataDirectory + customerCode + fileMutexSuffix;
            if (File.Exists(mutexFile))
                return false;
            try
            {
                using (var a = File.Create(mutexFile))
                {
                    a.Close();
                    File.SetAttributes(mutexFile, File.GetAttributes(mutexFile) | FileAttributes.Hidden);
                }
            }
            catch (DirectoryNotFoundException)
            {
                return false;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (IOException)
            {
                return false;
            }
            lockedCustomerCode = customerCode;
            return true;
        }

        void UnlockFile(string customerCode)
        {
            string mutexFile = CustomerDataDirectory + customerCode + fileMutexSuffix;
            if (File.Exists(mutexFile))
                File.Delete(mutexFile);
            lockedCustomerCode = "";
        }

        void UnlockLastFile()
        {
            if (lockedCustomerCode != null && lockedCustomerCode != "")
                UnlockFile(lockedCustomerCode);
        }

        void SetInputControls(bool enabled = true)
        {
            foreach (var c in GetAll(this, "InputControlTag"))
                c.Enabled = enabled;
        }

        public IEnumerable<Control> GetAll(Control control, string tag)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, tag))
                                      .Concat(controls)
                                      .Where(c => c.Tag != null && c.Tag.ToString() == tag);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            UnlockLastFile();
        }

        private void ξεκλείδωμαΠελάτηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Είστε βέβαιος πως θέλετε να επιβάλλετε το ξεκλείδωμα του πελάτη; Εάν ο πελάτης είναι ανοιχτός σε άλλον υπολογιστή, ενδέχεται να χαθούν δεδομένα.", "Ξεκλείδωμα Πελάτη", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                UnlockFile(currentCustomerCode);
                SetInputControls(true);
            }
        }

        private void dataGridViewCoupons_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            //if(e.ColumnIndex)
            //dataGridViewCoupons.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null;
            e.ThrowException = false;
            e.Cancel = false;
        }

        private void backupΣεΦάκελοToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var res = folderBrowserDialogNetwork.ShowDialog();
            if (res == DialogResult.OK)
            {
                DirectoryCopy(CustomerDataDirectory, folderBrowserDialogNetwork.SelectedPath, true);
                string[] files = Directory.GetFiles(folderBrowserDialogNetwork.SelectedPath, "*.lock");
                foreach (var f in files)
                    File.Delete(f);
                MessageBox.Show("Έγινε Backup στη θέση\n\t" + folderBrowserDialogNetwork.SelectedPath, "Το Backup Ολοκληρώθηκε");
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                try
                {
                    file.CopyTo(temppath, true);
                }
                catch (Exception)
                {
                    MessageBox.Show("Ο φάκελος προορισμού χρησιμοποιείται από άλλο πρόγραμμα. Κλείστε το και προσπαθήστε ξανά.", "Η Επαναφορά Απέτυχε", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        private void επαναφοράΑπόBackupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var res = folderBrowserDialogNetwork.ShowDialog();
            if (res == DialogResult.OK)
            {
                string selDir = folderBrowserDialogNetwork.SelectedPath;
                string metaDir = selDir + @"Metadata\";
                if (Directory.Exists(metaDir))
                    Directory.Delete(metaDir);
                DirectoryCopy(selDir, CustomerDataDirectory, true);
                MessageBox.Show("Έγινε επαναφορά από το Backup στη θέση\n\t" + selDir, "Η Επαναφορά Ολοκληρώθηκε");
                Application.Exit();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (Directory.Exists(CustomerDataDirectory))
            {
                customerMetadataList = new List<CustomerMetadata>();
                string[] fileList = Directory.GetFiles(CustomerDataDirectory);
                foreach (var file in fileList)
                {
                    FileInfo f = new FileInfo(file);
                    if (f != null && f.Length > 0)
                    {
                        CustomerMetadata m = FileAnalysisNotMetadata(Path.GetFileName(file));
                        if (m != null)
                            customerMetadataList.Add(m);
                    }
                }
                //var today = DateTime.Today;
                //var endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
                //FindExpiredProducts(endOfMonth, customerMetadataList);
                var q = customerMetadataList.Select(x => x.code);
                if (q != null && q.Count() > 0)
                {
                    string sumString = "";
                    List<Customer> tempCustList = new List<Customer>();
                    foreach (string s in q)
                    {
                        var tempCustomer = FarnetDataContext1.Customers.Where(x => x.PE_CODE == s).FirstOrDefault();
                        if (tempCustomer != null)
                            tempCustList.Add(tempCustomer);
                    }
                    if (tempCustList.FirstOrDefault() != null)
                    {
                        CustomerList c = new CustomerList(tempCustList);
                        c.ShowDialog();
                        if (c.DialogResult == DialogResult.OK)
                        {
                            if (c.selectedCustomer == null)
                                MessageBox.Show("γιατί δυνέβη αυτό;;;;");
                            else
                            {
                                LoadCustomer(c.selectedCustomer);
                            }
                        }
                    }/*
                    foreach (var t in tempCustList.OrderBy(x => x.PE_EPWNYMIA))
                        sumString += "\n  " + t.PE_CODE + "\t\t" + t.PE_EPWNYMIA + " " + t.PE_ONOMA;
                    MessageBox.Show("Οι ακόλουθοι πελάτες έχουν εκκρεμείς οφειλές:\n" + sumString);*/
                }
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            var item = (Product)comboBox1.SelectedItem;
            if (item != null)
            {
                string barcode;
                if (item.ProductBarcodes.FirstOrDefault() == null)
                    barcode = "0000000000000";
                else
                    barcode = item.ProductBarcodes.FirstOrDefault().BRAP_AP_BARCODE;
                barcodeFound = true;
                barcodeTextBox.Text = barcode;
                var l = new List<Product>();
                l.Add(item);
                productBindingSource.DataSource = l.AsQueryable();
                tempProduct = item;
                //productBarcodeBindingSource.DataSource = b;
                //productBarcodeBindingSource.DataSource = FarnetDataContext1.Products.Where(x => x.AP_CODE == item.AP_CODE);
                priceTextBox.Text = item.AP_TIMH_LIAN.GetValueOrDefault().ToString("0.##"); ;
                SelectNextControl(barcodeTextBox, true, true, true, true);
            }
        }

        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && comboBox1.SelectedIndex > 0)
            {
                var item = (Product)comboBox1.SelectedItem;
                if (item != null)
                {
                    string barcode;
                    if (item.ProductBarcodes.FirstOrDefault() == null)
                        barcode = "0000000000000";
                    else
                        barcode = item.ProductBarcodes.FirstOrDefault().BRAP_AP_BARCODE;
                    barcodeFound = true;
                    barcodeTextBox.Text = barcode;
                    var l = new List<Product>();
                    l.Add(item);
                    productBindingSource.DataSource = l.AsQueryable();
                    tempProduct = item;
                    //productBarcodeBindingSource.DataSource = b;
                    //productBarcodeBindingSource.DataSource = FarnetDataContext1.Products.Where(x => x.AP_CODE == item.AP_CODE);
                    priceTextBox.Text = item.AP_TIMH_LIAN.GetValueOrDefault().ToString("0.##"); ;
                    SelectNextControl(barcodeTextBox, true, true, true, true);
                }
            }
        }
    }
}

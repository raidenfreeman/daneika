﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DGVPrinterHelper;

namespace GkinisPetros
{
    public partial class CustomerList : Form
    {
        public Customer selectedCustomer { get; private set; }
        SortableBindingList<Customer> displayedCustomerList { get; set; }
        public CustomerList(List<Customer> customersToDisplay)
        {
            displayedCustomerList = new SortableBindingList<Customer>(customersToDisplay.OrderBy(x=>x.PE_EPWNYMIA));
            selectedCustomer = null;
            InitializeComponent();
            dataGridView1.DataSource = displayedCustomerList;
        }

        private void CustomerList_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (selectedCustomer != null)
                this.DialogResult = DialogResult.OK;
            else
                this.DialogResult = DialogResult.Cancel;
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell != null && dataGridView1.CurrentCell.RowIndex > -1)
                selectedCustomer = displayedCustomerList[dataGridView1.CurrentCell.RowIndex];
        }

        private void εκτύπωσηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoPrinting(false);
        }

        void DoPrinting(bool keepColor)
        {
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Πελάτες με Εκκρεμείς Οφειλές";

            printer.PageNumbers = true;

            printer.PageNumberInHeader = false;

            printer.PorportionalColumns = true;

            printer.HeaderCellAlignment = StringAlignment.Near;

            printer.Footer = "Φαρμακείο Γκίνης Πέτρος\t\t" + DateTime.Today.ToShortDateString();

            printer.FooterSpacing = 15;

            if (!keepColor)
            {
                Color temp1 = dataGridView1.DefaultCellStyle.BackColor;
                Color temp2 = dataGridView1.AlternatingRowsDefaultCellStyle.BackColor;
                dataGridView1.DefaultCellStyle.BackColor = Color.White;
                dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.White;

                printer.PrintDataGridView(dataGridView1);

                dataGridView1.DefaultCellStyle.BackColor = temp1;
                dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = temp2;
            }
            else
                printer.PrintDataGridView(dataGridView1);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index > -1)
                selectedCustomer = displayedCustomerList[index];
            this.Close();
        }

        private void εκτύπωσηΌπωςΦαίνεταιToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoPrinting(true);
        }
    }
}
